package mpholetsiki.mentorship.annotations.hibernateexercise.customer;

import java.util.HashMap;
import java.util.Map;

import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.annotations.DomainProperty;
import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.annotations.DomainClass;
import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.annotations.DomainTransient;
import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.logic.AutoInsert;

@DomainClass
public class Customer
    implements AutoInsert {

  @DomainProperty(primaryKey = true, length = 500, allowNull = false, unique = true)
  private String customerNum;

  @DomainProperty(allowNull = false)
  private String name;

  @DomainProperty(allowNull = false, length = 100)
  private String surname;

  @DomainProperty(allowNull = true)
  private Integer dateOfBirth;

  @DomainTransient
  private int age;

  public Customer(String customerNum, String name, String surname, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  public int getAge() {
    return age;
  }

  @Override
  public String toString() {
    return "(customerNum=" + customerNum + ", " + "name=" + name + ", " + "surname=" + surname + ", " + "dateOfBirth="
        + dateOfBirth + ", age=" + age + ")";
  }

  @Override
  public Map toHashMap() {
    Map map = new HashMap();
    map.put("customerNum", "\'" + customerNum + "\'");
    map.put("name", "\'" + name + "\'");
    map.put("surname", "\'" + surname + "\'");
    map.put("dateOfBirth", dateOfBirth);
    map.put("age", age);
    return map;
  }
}
