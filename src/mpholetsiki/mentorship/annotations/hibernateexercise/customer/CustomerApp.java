package mpholetsiki.mentorship.annotations.hibernateexercise.customer;

import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.logic.HibernateSQL;

public class CustomerApp {

  public static void deploy() {
    startHibernate();
  }

  public static void startup() {
    saveCustomer();
    // saveAccount();
  }

  private static void startHibernate() {
    Class[] classes = getClasses();
    HibernateSQL.run(classes);
  }

  private static Class[] getClasses() {
    Class[] classes = new Class[] {Customer.class, Account.class};
    return classes;
  }

  private static void saveCustomer() {
    Customer customer1 = new Customer("CUST01", "Mpho", "l", 900429);
    HibernateSQL.run(Customer.class, customer1.toHashMap());

    Customer customer2 = new Customer("CUST02", "Mike", "m", 900429);
    HibernateSQL.run(Customer.class, customer2.toHashMap());

    Customer customer3 = new Customer("CUST03", "Thapelo", "t", 900429);
    HibernateSQL.run(Customer.class, customer3.toHashMap());

    Customer customer4 = new Customer("CUST04", "Noshad", "n", 900429);
    HibernateSQL.run(Customer.class, customer4.toHashMap());
  }

  private static void saveAccount() {
    Account account1 = new Account("ACC01", "CUST01", 2000);
    HibernateSQL.run(Account.class, account1.toHashMap());

    Account account2 = new Account("ACC02", "CUST01", 500);
    HibernateSQL.run(Account.class, account2.toHashMap());
  }
}
