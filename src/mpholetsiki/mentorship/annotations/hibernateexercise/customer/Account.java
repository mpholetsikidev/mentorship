package mpholetsiki.mentorship.annotations.hibernateexercise.customer;

import java.util.HashMap;
import java.util.Map;

import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.annotations.DomainProperty;
import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.logic.AutoInsert;
import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.annotations.DomainClass;

@DomainClass
public class Account
    implements AutoInsert {

  @DomainProperty(primaryKey = true, length = 500, allowNull = false, unique = true)
  private String accountNum;

  @DomainProperty(length = 500, allowNull = false, unique = true)
  private String customerNum;

  @DomainProperty(allowNull = false)
  private Integer balance;

  public Account(String accountNum, String customerNum, Integer balance) {
    super();
    this.accountNum = accountNum;
    this.customerNum = customerNum;
    this.balance = balance;
  }

  public String getAccountNum() {
    return accountNum;
  }

  public void setAccountNum(String accountNum) {
    this.accountNum = accountNum;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public void setCustomerNum(String customerNum) {
    this.customerNum = customerNum;
  }

  public Integer getBalance() {
    return balance;
  }

  public void setBalance(Integer balance) {
    this.balance = balance;
  }

  @Override
  public String toString() {
    return "(accountNum=" + accountNum + ", " + "customerNum=" + customerNum + ", " + "=" + balance + ")";
  }

  @Override
  public Map toHashMap() {
    Map map = new HashMap();
    map.put("accountNum", "\'" + accountNum + "\'");
    map.put("customerNum", "\'" + customerNum + "\'");
    map.put("balance", balance);
    return map;
  }
}
