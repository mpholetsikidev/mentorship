package mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Query {

  public static void runDdlStatement(String sqlQuery, Connection connection) {
    Statement statement;

    try {
      statement = connection.createStatement();
      statement.execute(sqlQuery);
      statement.close();
      connection.close();

      System.out.println("Database query successful");
    }
    catch (SQLException e) {
      e.printStackTrace();

    }
  }

  public static void runDmlStatement(String sqlQuery, Connection connection) {
    Statement statement;

    try {
      connection.prepareStatement(sqlQuery).execute();
      connection.close();

      System.out.println("Database query successful");
    }
    catch (SQLException e) {
      e.printStackTrace();

    }
  }
}
