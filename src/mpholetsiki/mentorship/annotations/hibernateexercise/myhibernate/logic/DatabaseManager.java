package mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

public class DatabaseManager {

  private static String sqlQuery;

  public static void generateDatabase(String tableName, Map tableFields) {
    Connection connection = connectToDatabase();
    sqlQuery = "DROP TABLE IF EXISTS ".concat(tableName).concat(";");
    sqlQuery += "CREATE TABLE ".concat(tableName).concat(toDdlString(tableFields));
    Query.runDdlStatement(sqlQuery, connection);
  }

  public static void insertIntoDatabase(String tableName, Map fieldData) {
    Connection connection = connectToDatabase();
    sqlQuery = "INSERT INTO ".concat(tableName).concat(toInsertString(fieldData)).concat(";");
    Query.runDmlStatement(sqlQuery, connection);
  }

  private static Connection connectToDatabase() {
    String connectionUrl = "jdbc:postgresql://localhost:5432/annotation";
    String username = "postgres";
    String password = "postgres";
    Connection connection = null;

    try {
      Class.forName("org.postgresql.Driver");
      connection = DriverManager.getConnection(connectionUrl, username, password);
    }
    catch (Exception e) {
      System.out.println("Database Connection Failed");
      e.printStackTrace();

      try {
        connection.close();
      }
      catch (SQLException e1) {
        System.out.println("Cannot Close Database Connection");
        e1.printStackTrace();
      }
    }
    return connection;
  }

  public static String toDdlString(Map tableFields) {
    return tableFields.toString().replace("{", "(").replace("}", ")").replace("=", " ").concat(";");
  }

  public static String toInsertString(Map fieldData) {
    return fieldData.keySet().toString().replace("[", "(").replace("]", ")") + " VALUES "
        + fieldData.values().toString().replace("[", "(").replace("]", ")");
  }
}