package mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.logic;

import java.util.Map;

public interface AutoInsert {
  public Map toHashMap();
}
