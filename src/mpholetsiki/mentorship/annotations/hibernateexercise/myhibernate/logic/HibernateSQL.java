package mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.logic;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.annotations.DomainProperty;
import mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.annotations.DomainClass;

public class HibernateSQL {

  private static Map domainFields = new HashMap();

  private static Map objectData;

  private HibernateSQL() {
  }

  // Running Hibernate for the first time (deploying)
  public static void run(Class[] classes) {
    for (Class classObject : classes) {
      if (isDomainClass(classObject)) {
        getDomainFieldAnnotations(classObject.getDeclaredFields(), "create");
      }

      if (domainFields.size() > 0) {
        DatabaseManager.generateDatabase(classObject.getSimpleName(), domainFields);
      }
    }
  }

  // Running Hibernate when creating an object
  public static void run(Class classObject, Map data) {
    objectData = data;
    if (isDomainClass(classObject)) {
      getDomainFieldAnnotations(classObject.getDeclaredFields(), "insert");

      if (domainFields.size() > 0) {
        DatabaseManager.insertIntoDatabase(classObject.getSimpleName(), domainFields);
      }
    }
  }

  private static boolean isDomainClass(Class domainClass) {
    return domainClass.isAnnotationPresent(DomainClass.class);
  }

  private static void getDomainFieldAnnotations(Field[] fields, String queryType) {
    for (Field field : fields) {
      Annotation fieldAnnotation = field.getAnnotation(DomainProperty.class);

      if (fieldAnnotation instanceof DomainProperty) {
        if (queryType == "create") {
          domainFields.put(field.getName(), getAnnotationProperties(fieldAnnotation, field));
        }

        if (queryType == "insert") {
          domainFields.put(field.getName(), objectData.get(field.getName()));
        }
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static String getAnnotationProperties(Annotation fieldAnnotation, Field field) {
    String fieldType = field.getType().getSimpleName();
    int fieldLength = ((DomainProperty) fieldAnnotation).length();
    boolean isFieldUnique = ((DomainProperty) fieldAnnotation).unique();
    boolean isPrimaryKey = ((DomainProperty) fieldAnnotation).primaryKey();
    boolean allowNull = ((DomainProperty) fieldAnnotation).allowNull();

    convertToDatabaseType(fieldType, fieldLength, isFieldUnique, isPrimaryKey, allowNull);

    return convertToDatabaseType(fieldType, fieldLength, isFieldUnique, isPrimaryKey, allowNull);
  }

  private static String convertToDatabaseType(String fieldType, int fieldLength, boolean isFieldUnique,
      boolean isPrimaryKey, boolean allowNull) {

    String databaseProperties = "";

    String[] databaseTypes = new String[] {"integer", "varchar"};
    String[] javaTypes = new String[] {"Integer", "String"};

    for (int i = 0; i < javaTypes.length; i++) {
      if (fieldType.equals(javaTypes[i])) {
        databaseProperties = databaseTypes[i];
        break;
      }
    }

    if (fieldType.equals("String")) {
      databaseProperties += "(" + fieldLength + ")";
    }

    if (isFieldUnique) {
      databaseProperties += " UNIQUE";
    }

    if (isPrimaryKey) {
      databaseProperties += " PRIMARY KEY";
    }

    if (allowNull && !isPrimaryKey) {
      databaseProperties += " NULL";
    }
    else {
      databaseProperties += " NOT NULL";
    }

    return databaseProperties;
  }
}