/**
 *
 * Class Description.
 *
 * @author Phoz<mpho@psybergate.co.za> 23 Mar 2015
 * 
 */
package mpholetsiki.mentorship.annotations.hibernateexercise.myhibernate.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Mpho
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DomainClass {
}
