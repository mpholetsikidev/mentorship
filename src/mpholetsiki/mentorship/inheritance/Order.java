package mpholetsiki.mentorship.inheritance;

public interface Order {

  public boolean isLessThan(Object object);

  public boolean isGreaterThan(Object object);
}
