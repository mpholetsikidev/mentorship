package mpholetsiki.mentorship.inheritance;

public abstract class Account {

  private int accountNum;

  private double balance;

  private String accountType;

  protected static double accountDailyLimit;

  public Account(int accountNum, double balance, String accountType) {
    try {
      this.validateAccount(accountNum, balance);
      this.accountNum = accountNum;
      this.balance = balance;
      this.accountType = accountType;
    }
    catch (InvalidAccountOpeningBalanceException e) {
    }

  }

  public int getAccountNum() {
    return accountNum;
  }

  public double getBalance() {
    return balance;
  }

  public String getAccountType() {
    return accountType;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  @SuppressWarnings("unused")
  private void hasOverdraftFacility() {

  }

  public static double getAccountDailyLimit() {
    return accountDailyLimit;
  }

  public static void setAccountDailyLimit(double accountDailyLimit) {
    Account.accountDailyLimit = accountDailyLimit;
  }

  public static void resetAccountDailyLimit() {

  }

  public abstract boolean isOverdrawn();

  public abstract boolean needsToBeReviewed();

  public abstract void validateAccount(int accountNum, double balance2)
      throws InvalidAccountOpeningBalanceException;

}
