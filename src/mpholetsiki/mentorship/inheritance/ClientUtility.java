package mpholetsiki.mentorship.inheritance;

public class ClientUtility {
  public static void printAccountBalances(Client client) {

    System.out.println("Client: " + client.getName() + ": " + client.getClientNum() + "\n");

    for (int i = 0; i < client.getAccounts().size(); i++) {

      System.out.println("Account Type: " + client.getAccounts().get(i).getAccountType());
      System.out.println("Account Number: " + client.getAccounts().get(i).getAccountNum());
      System.out.println("Balance: R" + client.getAccounts().get(i).getBalance());
      System.out.println("Overdrawn: " + client.getAccounts().get(i).isOverdrawn() + "\n");
    }
  }

  public static void withdrawMoneyFromSavings(Client client, int accountNumber, double amount) {
    double newBalance = 0;

    for (int i = 0; i < client.getAccounts().size(); i++) {
      if (client.getAccounts().get(i).getAccountNum() == accountNumber
          && client.getAccounts().get(i).getAccountType() == "Savings") {
        newBalance = client.getAccounts().get(i).getBalance() - amount;
        client.getAccounts().get(i).setBalance(newBalance);
        System.out.println("New account balane: " + client.getAccounts().get(i).getBalance());
        System.out.println("Overdrawn: " + client.getAccounts().get(i).isOverdrawn() + "\n");
        break;
      }
    }
  }
}
