package mpholetsiki.mentorship.inheritance;

import java.util.ArrayList;
import java.util.List;

public class Client {

  private String clientNum;

  private String name;

  private String address;

  private List<Account> accounts = new ArrayList<>();

  public Client(String clientNum, String name, String address) {
    this.clientNum = clientNum;
    this.name = name;
    this.address = address;
  }

  public String getName() {
    return name;
  }

  public String getClientNum() {
    return clientNum;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public List<Account> getAccounts() {
    return accounts;
  }

  public void addAccount(int accountNum, double balance, String accountType) {
    if (accountType.equals("Current")) {
      CurrentAccount account = new CurrentAccount(accountNum, balance, accountType);
      this.accounts.add(account);
    }
    else if (accountType.equals("Savings")) {
      SavingsAccount account = new SavingsAccount(accountNum, balance, accountType);
      if (account.getAccountType() != null) {
        this.accounts.add(account);
      }
    }
  }

  public void generateAccount(int numberOfAccounts, String accountType) {
    int randomAccountNumber = 0;
    double randomBalance = 0.0;

    for (int i = 1; i <= numberOfAccounts; i++) {
      randomAccountNumber = i;
      randomBalance += randomBalance + 1000;

      addAccount(randomAccountNumber, randomBalance, accountType);
    }
  }
}
