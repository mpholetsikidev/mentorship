package mpholetsiki.mentorship.inheritance;

public class CurrentAccount extends Account
    implements Order {

  public CurrentAccount(int accountNum, double balance, final String accountType) {
    super(accountNum, balance, accountType);
  }

  @Override
  public boolean isOverdrawn() {
    if (getBalance() < -5000) {
      return needsToBeReviewed();
    }
    else
      return false;
  }

  @Override
  public boolean needsToBeReviewed() {
    return true;
  }

  @Override
  public boolean isLessThan(Object object) {
    CurrentAccount currentAccount = (CurrentAccount) object;
    return this.getBalance() < currentAccount.getBalance();
  }

  @Override
  public boolean isGreaterThan(Object object) {
    CurrentAccount currentAccount = (CurrentAccount) object;
    return this.getBalance() > currentAccount.getBalance();
  }

  @Override
  public void validateAccount(int accountNum, double balance)
      throws InvalidAccountOpeningBalanceException {
    // TODO Auto-generated method stub
  }
}
