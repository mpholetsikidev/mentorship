package mpholetsiki.mentorship.inheritance.homework.stopwatch2;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

public class StopwatchPanel {

  private static final int STOPWATCH_SEC = 1000;

  private JFrame frame;

  private Timer timer;

  private int seconds;

  private int minutes;

  private int hours;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          StopwatchPanel window = new StopwatchPanel();
          window.frame.setVisible(true);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the application.
   */
  public StopwatchPanel() {
    initialize();
  }

  /**
   * Initialize the contents of the frame.
   */
  private void initialize() {
    seconds = 0;

    frame = new JFrame();
    frame.setBounds(100, 100, 267, 244);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().setLayout(null);

    final JLabel lblTimerSeconds = new JLabel("00");
    lblTimerSeconds.setFont(new Font("Yu Gothic Light", Font.BOLD, 50));
    lblTimerSeconds.setBounds(183, 47, 60, 70);
    frame.getContentPane().add(lblTimerSeconds);

    final JLabel lblTimerMinutes = new JLabel("00");
    lblTimerMinutes.setFont(new Font("Yu Gothic Light", Font.BOLD, 50));
    lblTimerMinutes.setBounds(98, 47, 67, 70);
    frame.getContentPane().add(lblTimerMinutes);

    final JLabel lblTimerHours = new JLabel("00");
    lblTimerHours.setFont(new Font("Yu Gothic Light", Font.BOLD, 50));
    lblTimerHours.setBounds(10, 47, 60, 70);
    frame.getContentPane().add(lblTimerHours);

    timer = new Timer(STOPWATCH_SEC, new ActionListener() {
      public void actionPerformed(ActionEvent evt) {

        if (seconds < 10) {
          lblTimerSeconds.setText("0" + seconds + "");
        }
        else {
          lblTimerSeconds.setText(seconds + "");
        }

        if (minutes < 10) {
          lblTimerMinutes.setText("0" + minutes + "");
        }
        else {
          lblTimerMinutes.setText(minutes + "");
        }

        if (hours < 10) {
          lblTimerHours.setText("0" + hours + "");
        }
        else {
          lblTimerHours.setText(hours + "");
        }

        seconds++;
        if (seconds == 59) {
          minutes++;
          seconds = 0;
          if (minutes == 59) {
            minutes = 0;
            seconds = 0;
            hours++;
          }
        }
      }
    });

    JButton btnStart = new JButton("Start");
    btnStart.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        timer.start();
      }
    });
    btnStart.setBounds(20, 128, 89, 23);
    frame.getContentPane().add(btnStart);

    JButton btnStop = new JButton("Stop");
    btnStop.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        timer.stop();
      }
    });
    btnStop.setBounds(144, 128, 89, 23);
    frame.getContentPane().add(btnStop);

    JButton btnReset = new JButton("Reset");
    btnReset.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        seconds = 0;
        lblTimerSeconds.setText(seconds + "");
      }
    });
    btnReset.setBounds(144, 162, 89, 23);
    frame.getContentPane().add(btnReset);

    JButton btnContinue = new JButton("Continue");
    btnContinue.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        timer.start();
      }
    });
    btnContinue.setBounds(20, 162, 89, 23);
    frame.getContentPane().add(btnContinue);

    JLabel lblNewLabel = new JLabel("Stopwatch");
    lblNewLabel.setFont(new Font("Simplified Arabic", Font.PLAIN, 30));
    lblNewLabel.setBounds(62, 11, 140, 34);
    frame.getContentPane().add(lblNewLabel);

    JLabel lblNewLabel_1 = new JLabel(":");
    lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 50));
    lblNewLabel_1.setBounds(160, 44, 22, 70);
    frame.getContentPane().add(lblNewLabel_1);

    JLabel label = new JLabel(":");
    label.setFont(new Font("Tahoma", Font.BOLD, 50));
    label.setBounds(75, 44, 22, 70);
    frame.getContentPane().add(label);

  }
}
