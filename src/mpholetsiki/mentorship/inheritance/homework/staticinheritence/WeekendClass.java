package mpholetsiki.mentorship.inheritance.homework.staticinheritence;

public class WeekendClass extends Mentorship {

  private int fees;

  public WeekendClass(String topic, int fees) {
    super(topic);
    this.fees = fees;
  }

  public int getFees() {
    return fees;
  }

  public void setFees(int fees) {
    this.fees = fees;
  }

  public String getClassDetails() {
    // Static methods are inherited
    return getFullDetails();
  }

  @Override
  public int getPeopleAttending() {
    peopleAttending = 30;
    return peopleAttending;
  }

  public static int getPeopleAttendingStatic() {
    peopleAttendingStatic = 50;
    return peopleAttendingStatic;
  }

}
