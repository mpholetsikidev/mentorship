package mpholetsiki.mentorship.inheritance.homework.staticinheritence;

public class EveningClass extends Mentorship {
  private String minimunAge;

  public EveningClass(String topic, String minimunAge) {
    super(topic);
    this.minimunAge = minimunAge;
  }

  public String getMinimunAge() {
    return minimunAge;
  }

  public void setMinimunAge(String minimunAge) {
    this.minimunAge = minimunAge;
  }

  public String getClassDetails() {
    // Static methods are inherited
    return getFullDetails();
  }

  // @Override
  public static String getLocation() {
    return "Evening classes are no longer available";
  }
}
