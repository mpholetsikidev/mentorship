package mpholetsiki.mentorship.inheritance.homework.staticinheritence;

public abstract class Mentorship {
  private static String mentor;

  private static String location;

  public static int peopleAttendingStatic = 20;

  public int peopleAttending = 20;

  public String topic;

  static {
    mentor = "Jacob";
    location = "Rosebank";
  }

  public Mentorship(String topic) {
    super();
    this.topic = topic;
  }

  public static String getMentor() {
    return mentor;
  }

  public static void setMentor(String mentor) {
    Mentorship.mentor = mentor;
  }

  public static String getLocation() {
    return location;
  }

  public static void setLocation(String location) {
    Mentorship.location = location;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public static String getFullDetails() {
    return mentor + " " + location;
  }

  public int getPeopleAttending() {
    return peopleAttending;
  }

  public static int getPeopleAttendingStatic() {
    return peopleAttendingStatic;
  }
}
