package mpholetsiki.mentorship.inheritance.homework.staticinheritence;

public class Test {

  public static void main(String[] args) {
    WeekendClass wc = new WeekendClass("Java", 5000);

    System.out.println("Course Details: " + wc.getClassDetails());
    System.out.println("Topic: " + wc.getTopic() + " - R" + wc.getFees());

    System.out.println("\nCalling static method from object: " + wc.getPeopleAttendingStatic());
    System.out.println("Calling static method from object reference: " + wc.getPeopleAttendingStatic());

    System.out.println("\nCalling concrete method from object: " + wc.getPeopleAttending());
    System.out.println("Calling concrete method from object reference: " + wc.getPeopleAttending());

    // calling a static method polymorphically will not work
    // change weekend class mentor and get it
    // System.out.println("\nWeekend Class Old Mentor: " + wc.getMentor());
    // Mentorship.setMentor("Mpho");
    // System.out.println("Weekend Class New Mentor: " + wc.getMentor());

    // // change evening class mentor and get it
    // System.out.println("\nEvening Class Old Mentor: " + ec.getMentor());
    // ec.setMentor("Natasha");
    // System.out.println("Evening Class New Mentor: " + ec.getMentor());
    //
    // // change evening class location and get it
    // System.out.println("\nEvening Class Location: " + EveningClass.getLocation());

  }
}
