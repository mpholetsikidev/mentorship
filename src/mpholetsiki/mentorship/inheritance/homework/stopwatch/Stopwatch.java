package mpholetsiki.mentorship.inheritance.homework.stopwatch;

public class Stopwatch extends Watch {

  private long timeStarted;

  private long timeEnded;

  private float totalTime;

  public Stopwatch() {
    super();
  }

  public void startWatch() {
    timeStarted = getCurrentTime();
  }

  public void stopWatch() {
    timeEnded = getCurrentTime();
  }

  public float getTotalTime() {
    if (totalTime == 0) {
      totalTime = (timeEnded - timeStarted) / 1000f;
    }
    else {
      totalTime = totalTime + ((timeEnded - timeStarted) / 1000f);
    }

    return totalTime;
  }
}
