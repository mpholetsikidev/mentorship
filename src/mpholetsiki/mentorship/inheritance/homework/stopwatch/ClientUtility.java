package mpholetsiki.mentorship.inheritance.homework.stopwatch;

import java.util.Scanner;

public class ClientUtility {

  public static void start() {

    String command = null;

    Stopwatch stopwatch = new Stopwatch();

    System.out.println("'s' start the watch.");
    System.out.println("'o' stops the watch.");
    System.out.println("'c' continues the watch from previous time.");
    System.out.println("Any other key exits the watch.");
    System.out.print("\nType in your command: ");

    @SuppressWarnings("resource")
    Scanner scan = new Scanner(System.in);
    command = scan.nextLine();

    if (command.equalsIgnoreCase("s")) {
      stopwatch.startWatch();
      System.out.println("Started...");
      System.out.print("Enter 'o' to stop the watch: ");

      scan = new Scanner(System.in);
      command = scan.nextLine();

      if (command.equalsIgnoreCase("o")) {
        stopwatch.stopWatch();
        System.out.println("Stopped...");
        System.out.println("Total Time: " + stopwatch.getTotalTime() + " seconds.");
      }
    }

    scan = new Scanner(System.in);
    command = scan.nextLine();

    while (command.equalsIgnoreCase("c")) {
      stopwatch.startWatch();
      System.out.println("Continued...");
      System.out.print("Enter 'o' to stop the watch: ");

      scan = new Scanner(System.in);
      command = scan.nextLine();

      if (command.equalsIgnoreCase("o")) {
        stopwatch.stopWatch();
        System.out.println("Stopped...");
        System.out.println("Total Time: " + stopwatch.getTotalTime() + " seconds.");
      }

      scan = new Scanner(System.in);
      command = scan.nextLine();
    }

    System.out.println("Exit...");
  }
}
