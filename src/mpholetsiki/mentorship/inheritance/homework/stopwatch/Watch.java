package mpholetsiki.mentorship.inheritance.homework.stopwatch;

public abstract class Watch {

  public long currentTime;

  public Watch() {

  }

  public long getCurrentTime() {
    return System.currentTimeMillis();
  }
}
