package mpholetsiki.mentorship.inheritance;

public class Test {
  public static void main(String[] args) {

    Client client = new Client("c1", "Mpho", "1212 street");
    client.generateAccount(3, "Current");
    client.generateAccount(5, "Savings");

    ClientUtility.printAccountBalances(client);

    ClientUtility.withdrawMoneyFromSavings(client, 3, 6000.00);

    // Compare Current Account Balances
    // System.out.println(">>>>>  Is account1 less than account2: " + currentAccount1.isLessThan(currentAccount2));
    // System.out.println(">>>>>  Is account3 greater than account4: " +
    // currentAccount3.isGreaterThan(currentAccount4));
  }
}