package mpholetsiki.mentorship.inheritance.exercise3;

public class TypeCompatibiltyCompilerTest {

  public static void main(String[] args) {
    TypeCompatibilityClass compatible = new TypeCompatibilityClass2();
    System.out.println(">>>>> createOrder(); ran from TypeCompatibilityClass2");
    compatible.createOrder();

    System.out.println("\n>>>>> getOrder(); ran from TypeCompatibilityClass2");
    compatible.getOrder();

    System.out.println("\n>>>>> getName(); from TypeCompatibilityClass does not seem to run");
    compatible.getName();

    System.out.println("\n>>>>> isAccountNumberValid(); from TypeCompatibilityClass does not seem to run");
    compatible.isAccountNumberValid();

  }

}
