/**
 * Object Reference can be of type Class or Interface
 * Object must be a Concrete Class
 */
package mpholetsiki.mentorship.inheritance.exercise3;

public class TypeCompatibilityTest {

  public static void main(String[] args) {
    System.out.println(">>>>> 111 ");
    checkAbstractCompatibility();
  }

  public static int checkAbstractCompatibility() {

    // testing abstract class instatiation
    TypeCompatibilityAbstract tca = new TypeCompatibilityAbstract(12) {

      @Override
      public int reverseOrderNumber() {
        System.out.println(">>>>> 222 ");
        return 111;
      }

    };
    return 001;
  }

  public int reverseOrderNumber() {
    System.out.println(">>>>> 333 ");
    return 111;
  }

  public static void checkCompatibilty() {
    TypeCompatibilityClass compatible11 = new TypeCompatibilityClass("Mpho", "acc123");

    //
    // TypeCompatibilityClass uncompatible12 = new TypeCompatibilityAbstract();
    //
    // TypeCompatibilityClass uncompatible13 = new TypeCompatibilityInterface();

    TypeCompatibilityInterface compatible21 = new TypeCompatibilityClass("Mpho", "acc123");

    // TypeCompatibilityInterface uncompatible22 = new TypeCompatibilityAbstract();
    //
    // TypeCompatibilityInterface uncompatible23 = new TypeCompatibilityInterface();
    //
    // TypeCompatibilityAbstract uncompatible31 = new TypeCompatibilityClass("Mpho", "acc123");
    //
    // TypeCompatibilityAbstract uncompatible32 = new TypeCompatibilityAbstract(122);
    //
    // TypeCompatibilityAbstract uncompatible33 = new TypeCompatibilityInterface();

  }
}
