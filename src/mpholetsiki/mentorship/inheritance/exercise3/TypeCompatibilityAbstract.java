package mpholetsiki.mentorship.inheritance.exercise3;

/*
 * An abstract class can have both normal methods and abstract method.
 */
public abstract class TypeCompatibilityAbstract {
  private int orderNumber;

  public int getOrderNumber() {
    return orderNumber;
  }

  // apparently abstract classes cannot be instantiated
  public TypeCompatibilityAbstract(int orderNumber) {
    this.orderNumber = orderNumber;
  }

  public abstract int reverseOrderNumber();

  public int unReverseOrderNumber() {
    return 0;
  }
}
