package mpholetsiki.mentorship.inheritance.exercise3;

public class TypeCompatibilityClass
    implements TypeCompatibilityInterface {
  private String name;

  private String accountNumber;

  public TypeCompatibilityClass(String name, String accountNumber) {
    this.name = name;
    this.accountNumber = accountNumber;
  }

  public boolean isAccountNumberValid() {
    return accountNumber != null;
  }

  public String getName() {
    return name;
  }

  @Override
  public void getOrder() {
    System.out.println("Order from class1 created");
  }

  @Override
  public void createOrder() {
    System.out.println("Orderr from class1 created");
  }
}
