package mpholetsiki.mentorship.inheritance.exercise3;

public interface TypeCompatibilityInterface {
  public void getOrder();

  public void createOrder();
}
