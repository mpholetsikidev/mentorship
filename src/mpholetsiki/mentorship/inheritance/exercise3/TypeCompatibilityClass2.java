package mpholetsiki.mentorship.inheritance.exercise3;

public class TypeCompatibilityClass2 extends TypeCompatibilityClass {

  public TypeCompatibilityClass2() {
    super("Mpho", "acc555");
  }

  public void getOrder() {
    System.out.println("Order from class2 exists");
  }

  public void createOrder() {
    System.out.println("Order from class2 created");
  }
}
