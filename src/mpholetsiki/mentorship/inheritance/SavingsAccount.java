package mpholetsiki.mentorship.inheritance;

public class SavingsAccount extends Account {

  public SavingsAccount(int accountNum, double balance, String accountType) {
    super(accountNum, balance, accountType);

  }

  @Override
  public boolean isOverdrawn() {

    // System.out.println(">>>>> " + (Account) accountNum);

    if (getBalance() < 5000) {

      return needsToBeReviewed();
    }
    else
      return false;

  }

  @Override
  public boolean needsToBeReviewed() {
    return true;
  }

  @Override
  public void validateAccount(int accountNum, double balance)
      throws InvalidAccountOpeningBalanceException {
    if (balance < 5000) {
      System.out.println(">>>>> Cannot Open Account with a balance less than 5000: Account Number: " + accountNum
          + " Balance: " + balance + "\n");
      throw new InvalidAccountOpeningBalanceException();
    }
  }
}
