package mpholetsiki.mentorship.java.language;

public class Initializers {
	public static String name;

	public static void main(String args[]) {
		System.out.println("� main");
		InitializersTest initClass = new InitializersTest();
		initClass.methodRun();
	}

	static {
		name = "Java";
		System.out
				.println("� static initializer"
						+ "\n\t**NOTE:"
						+ "\n\tRuns when class loads, that is, when a class is referenced for the firt time.");
	}

//	 static throws RuntimeException {
//	 System.out.println("WHATS RUNNING > Static Initializer2");
//	 throw new RuntimeException();
//	 // var = 1/0;
//	 }
}
