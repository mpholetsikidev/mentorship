package mpholetsiki.mentorship.java.language;

public class ObjectsTypesAndLiterals {
	
	public static String oVariable = "A variable that varies per object.\n"
			+ "e.g. in Customer, an object variable would be name.\n"
			+ "This is an object variable and is accessible by any method\nand is being printed from message()";

	public static void main(String[] args) {
		JavaSpecClass2.exploreSizes();
		message(oVariable);
	}
	
	ObjectsTypesAndLiterals(){
		System.out.println("CONSTRUCTOR:\nThis constructor does not have scope \n"
				+ "and therefore inherits the modifier of a class.");
	}
	
	public static String message(String oVariable) {
		String lVariable = "These variables are declared local to a method.\n"
				+ "They have to be initialsed, unlike object variables that have default assignments."
				+ "This is a local variable and is accessible only from message()";
		
		System.out.println(oVariable);
		System.out.println();
		System.out.println(lVariable);
		
		return lVariable;
	}
	
	static class JavaSpecClass2 {
		public static void exploreSizes() {
			Byte bmin = Byte.MIN_VALUE;
			Byte bmax = Byte.MAX_VALUE;
			Short smin = Short.MIN_VALUE;
			Short smax = Short.MAX_VALUE;
			Integer imin = Integer.MIN_VALUE;
			Integer imax = Integer.MAX_VALUE;
			Long lmin = Long.MIN_VALUE;
			Long lmax = Long.MAX_VALUE;
			
			System.out.println("Minimum BYTE value: \t" + bmin);
			System.out.println("Maximin BYTE value: \t" + bmax + "\n");
			System.out.println("Minimum SHORT value: \t" + smin);
			System.out.println("Maximin SHORT value: \t" + smax + "\n");
			System.out.println("Minimum INTEGER value: \t" + imin);
			System.out.println("Maximin INTEGER value: \t" + imax + "\n");
			System.out.println("Minimum LONG value: \t" + lmin);
			System.out.println("Maximin LONG value: \t" + lmax + "\n");
		}
	}
}


