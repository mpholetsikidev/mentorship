package mpholetsiki.mentorship.java.language.objectcountstaticvariableexample;

public class Employee1 {

  private static int employeeCount = 0;

  public Employee1() {
    incrementEmployeeCount();
  }

  private void incrementEmployeeCount() {
    employeeCount++;
  }

  public static void printEmployeeCount() {
    System.out.println("Number of employees: " + employeeCount);
  }

  public static void main(String[] args) {
    new Employee1();
    new Employee1();
    new Employee1();
    new Employee1();

    printEmployeeCount();
  }
}



