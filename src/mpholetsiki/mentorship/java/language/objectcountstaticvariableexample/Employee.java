package mpholetsiki.mentorship.java.language.objectcountstaticvariableexample;

public class Employee {

  private static int employeeCount;

  public Employee() {
    int num1 = 10;
    int increment;
    int decrement;

    increment = num1 + 1;
    System.out.println("10 + 1: " + increment);

    increment = num1++;
    System.out.println("num1++: " + increment);

    increment = num1++;
    System.out.println("num1++: " + increment);

    decrement = num1 - 1;
    System.out.println("10 - 1: " + decrement);

    decrement = num1--;
    System.out.println("num1--: " + decrement);

    decrement = num1--;
    System.out.println("num1--: " + decrement);

    //

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

  }
  static {
    employeeCount = 0;
  }

  {
    employeeCount++;
  }

  public static void printEmployeeCount() {
    // System.out.println("Number of employees: " + employeeCount);
  }

  public static void main(String[] args) {
    new Employee();
    // new Employee();
    // new Employee();
    // new Employee();

    printEmployeeCount();
  }

}
