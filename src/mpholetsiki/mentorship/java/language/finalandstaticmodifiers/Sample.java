package mpholetsiki.mentorship.java.language.finalandstaticmodifiers;

public class Sample {
	
	// A static field doesn't have to be initialized
	public static String name;
	// A final field may not be blank unless if its initialized inside the
	// constructor
	public final String surname = "Letsiki";
	public final String surname2;
	
	public static final String gender;

	public Sample(){
		surname2 = "Letsiki2";
	}
	
	static {
		gender = "female";
	}
	
	public static void main(String[] args) {

	}

}
