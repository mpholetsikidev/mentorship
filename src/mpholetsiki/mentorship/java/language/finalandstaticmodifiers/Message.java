package mpholetsiki.mentorship.java.language.finalandstaticmodifiers;

public class Message {
	public static void main(String[] args){
		String finalMethodMessage = finalMethod();
		System.out.println("Calling a final method from inside a final class:\n\n" + finalMethodMessage);
	}
	
	public final static String finalMethod (){
		String message = "A final method cannot be overridden or hidden by subclasses.\n"
				+ "This is used to prevent unexpected behavior from a subclass altering a \n"
				+ "method that may be crucial to the function or consistency of the class.\n\n"
				+ "This method is final cannot be overidden";
		
		return message;
	}
}
