package mpholetsiki.mentorship.java.language.finalandstaticmodifiers;

public final class ModifierFinalClass {

	public static void main(String args[]) {
		String description = "A final class cannot be subclassed/Extended. Doing this can confer security and \n"
				+ "efficiency benefits, so many of the Java standard library classes \n"
				+ "are final, such as java.lang.System and java.lang.String.\n\n" 
				+ "Note:\n" 
				+ "All methods in a final class are implicitly final.\n\n"
				+ "Example:\n"
				+ "public final class MyFinalClass {...}\n"
				+ "public class ThisIsWrong extends MyFinalClass {...} // forbidden\n\n"
				+ "Error:\n"
				+ "The type extendFinal cannot subclass the final class ModifierFinalClass";
		
		System.out.println(description);
	}

//	 public class extendFinal extends ModifierFinalClass {
//	
//	 }

}
