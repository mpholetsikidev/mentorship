package mpholetsiki.mentorship.java.language;

public class OperatorSwitch {
	public static void main(String[] args) {
		String description = "DESRIPTION\n"
				+ "Unlike if-then and if-then-else statements, the switch\n"
				+ "statement can have a number of possible execution paths.\n"
				+ "A switch works with the byte, short, char, and int primitive data types. \n"
				+ "It also works with enumerated types (discussed in Enum Types), \n"
				+ "the String class, and a few special classes that wrap certain \n"
				+ "primitive types: Character, Byte, Short, and Integer (discussed in Numbers and Strings).\n\n"
				+ "RUN\n";

		System.out.println(description);

		String month = "April";
		String holiday = "";
		
		switch (month) {
		case "April":
			holiday = "Good Friday";
			break;
		case "January":
			holiday = "No holidays";
			break;
		default:
			holiday = "Month not selected";
			break;
		}

		System.out.println(month + ": " + holiday);
	}
}
