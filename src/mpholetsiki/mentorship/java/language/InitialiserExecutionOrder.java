package mpholetsiki.mentorship.java.language;

public class InitialiserExecutionOrder {

  public InitialiserExecutionOrder() {
    System.out.println("constructor called");
  }

  static {
    System.out.println("static initializer called");
  }

  {
    System.out.println("instance initializer called");
  }

  public static void main(String[] args) {
    new InitialiserExecutionOrder();
    new InitialiserExecutionOrder();
  }
}