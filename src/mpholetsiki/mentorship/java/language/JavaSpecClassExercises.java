package mpholetsiki.mentorship.java.language;

import java.util.ArrayList;

//----- Static means ===== all objects accesses the same, whether you change stuff or edit, if you access it in another class it will get the changed object.
public class JavaSpecClassExercises {
	
	public static String name;
	public static ArrayList<String> list = new ArrayList<String>(); 
	private String surname;
	
	//largest integer numbers: 32bits
	String pos = new String("123");
	int neg = -2147483648;
	
	
	
	//----- STATIC INTIALIZER/BLOCK: This executes on class load. Can write code inside.
	static {
		name = "Kea";
		list.add("Mpho");
	}
	
	//----- INSTANCE INITIALIZER/BLOCK: Run as a first line of every constructor. i.e. When the object is initialized. Can write code inside.
	{
		surname = "Letsiki";
	}
	
	public JavaSpecClassExercises(){
		System.out.print(surname);
	}
	
	public static void main (String args[]) {
//		System.out.println(Integer.MIN_VALUE);
//		
////		2147483647
//		
//		System.out.println(0x80000000);
		
		m1();
	}
	
	public static void m1() {
	Integer a = 1;
	Integer b = 1;
	Integer c = -130;
	Integer d = -130;
	
	System.out.println(a == b);
	System.out.println(c == d);
	
	System.out.println(Integer.valueOf(c));
	System.out.println(Integer.valueOf(d));
	}
}
