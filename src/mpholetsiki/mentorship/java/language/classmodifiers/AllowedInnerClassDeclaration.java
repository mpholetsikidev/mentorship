package mpholetsiki.mentorship.java.language.classmodifiers;

public class AllowedInnerClassDeclaration {

  private abstract class PrivateAbstractClass {

    @SuppressWarnings("unused")
    public final static String NAME = "I'm a static variable";

  }

  private final class PrivateFinalClass {

  }

  protected class ProtectedClass {

  }
}
