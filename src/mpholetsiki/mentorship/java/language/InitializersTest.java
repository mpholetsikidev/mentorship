package mpholetsiki.mentorship.java.language;

public class InitializersTest {

	{
		System.out.println("� instance initializer" + "\n\t**NOTE"
				+ "\n\tExecutes on the first line of a constructor.");
	}

	public InitializersTest() {
		System.out.println("� constructor");
	}

	public void methodRun() {
		System.out.println("� method");
	}
}
