package mpholetsiki.mentorship.java.language;

public class OperatorTenary {
	public static void main (String[] args){
		String description = "DESRIPTION\n"
				+ "This operator consists of three operands \n"
				+ "and is used to evaluate Boolean expressions. \n"
				+ "The goal of the operator is to decide \n"
				+ "which value should be assigned to the variable. \n\n"
				+ "The operator is written as:\n"
				+ "variable x = (expression) ? value if true : value if false\n\n"
		    	+ "RUN\n";
		
		System.out.println(description);
		
		int a = 10;
		int b = 20;

		b = (b == a) ? 20: 30;
		System.out.println( ">> Value of b is : " +  b );

		b = (a == 10)  ? 20: 30;
		System.out.println( ">> Value of b is : " + b );
	}
}
