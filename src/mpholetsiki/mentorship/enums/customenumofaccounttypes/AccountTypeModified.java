package mpholetsiki.mentorship.enums.customenumofaccounttypes;

public class AccountTypeModified {

  public static final AccountTypeModified CURRENT = new AccountTypeModified();

  public static final AccountTypeModified SAVINGS = new AccountTypeModified();

  private AccountTypeModified() {
  }
}