package mpholetsiki.mentorship.enums.customenumofaccounttypes;

public class AccountType {

  public static final AccountType CURRENT = new AccountType(1);

  public static final AccountType SAVINGS = new AccountType(2);

  private int accountType;

  private AccountType() {
  }

  private AccountType(int accountType) {
    this.accountType = accountType;
  }
}