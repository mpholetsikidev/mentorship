package mpholetsiki.mentorship.enums;

public class Account {

  public AccountType acc;

  
  public Account(AccountType acc) {
    super();
    this.acc = acc;
  }


  public void printStuff() {
    System.out.println(">>>>> " + acc.ordinal());
    // System.out.println(">>>>> " + acc.values());
    // System.out.println(">>>>> " + acc.valueOf("CREDIT"));
  }

}
