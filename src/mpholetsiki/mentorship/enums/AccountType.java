package mpholetsiki.mentorship.enums;

public enum AccountType {
  CURRENT_ACCOUNT,
  CREDIT_ACCOUNT,
  SAVINGS_ACCOUNT;
}
