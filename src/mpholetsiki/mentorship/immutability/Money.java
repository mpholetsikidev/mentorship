package mpholetsiki.mentorship.immutability;

public class Money {

  private int cents;

  public Money(int cents) {
    this.cents = cents;
  }

  public int add(Money money) {
    return cents + money.cents;
  }

  public static void main(String[] args) {

    Money m1 = new Money(1000);
    Money m3 = new Money(3000);

    m3.add(m1);

    System.out.println(">>>>> " + m3.cents);
  }

}
