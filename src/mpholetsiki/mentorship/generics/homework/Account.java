package mpholetsiki.mentorship.generics.homework;

public class Account {
  private String accountNumber;

  private double balance;

  public Account(String accountNumber, double balance) {
    super();
    this.accountNumber = accountNumber;
    this.balance = balance;
  }

  public Account() {
  }

  public boolean isOverdraft() {
    return balance < 0;
  }
}
