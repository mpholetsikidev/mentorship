package mpholetsiki.mentorship.generics.homework;

public class Student extends Person {

  public int yearRegistered;

  public Student(String name, int age) {
    super(name, age);
  }

  public int getYearRegistered() {
    return yearRegistered;
  }

  public void setYearRegistered(int yearRegistered) {
    this.yearRegistered = yearRegistered;
  }
}
