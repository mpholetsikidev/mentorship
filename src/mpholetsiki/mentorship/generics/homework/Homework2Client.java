package mpholetsiki.mentorship.generics.homework;

import psybergate.mentorship.generics.homework.Homework2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Homework2Client {
  public static void main(String[] args) {
    callWithStrings();
    callWithIntegers();
  }

  private static void callWithIntegers() {
    List<Integer> numbers = new ArrayList<Integer>();
    numbers.add(1251);
    numbers.add(234);
    numbers.add(4534);
    numbers.add(1535251);
    numbers.add(454);
    numbers.add(344);
    numbers.add(56456);

    System.out.println("ALL NUMBERS");
    for (Iterator iter = numbers.iterator(); iter.hasNext();) {
      System.out.println(">>>>> " + iter.next());
    }

    Homework2 homework2 = new Homework2();
    List<Integer> numbersGreaterThan1000 = homework2.greaterThan1000(numbers);

    System.out.println("\nNUMBERS GREATER THAN 1000");
    for (Iterator iter = numbersGreaterThan1000.iterator(); iter.hasNext();) {
      System.out.println(">>>>> " + iter.next());
    }
  }

  private static void callWithStrings() {
    List<String> strings = new ArrayList<String>();
    strings.add("1251");
    strings.add("234");
    strings.add("4534");
    strings.add("1535251");
    strings.add("454");
    strings.add("344");
    strings.add("56456");

    System.out.println("ALL STRINGS");
    for (Iterator iter = strings.iterator(); iter.hasNext();) {
      System.out.println(">>>>> " + iter.next());
    }

    // Homework2 homework2 = new Homework2();
    // List<Integer> numbersGreaterThan1000 = homework2.greaterThan1000(strings);
    //
    // System.out.println("\nSTRINGS GREATER THAN 1000");
    // for (Iterator iter = numbersGreaterThan1000.iterator(); iter.hasNext();) {
    // System.out.println(">>>>> " + iter.next());
    // }
  }
}
