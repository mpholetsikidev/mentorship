package mpholetsiki.mentorship.generics.homework;

import java.util.ArrayList;
import java.util.List;

public class Homework3 {

  /**
   * 
   * @param collection
   * @return the number of even numbers in the collection
   */
  private static int evenIntegerCount(List<Integer> collection) {
    int evenIntegerCount = 0;

    for (int i = 0; i < collection.size(); i++) {
      if (collection.get(i) % 2 == 0) {
        evenIntegerCount += 1;
      }
    }
    return evenIntegerCount;
  }

  /**
   * 
   * @param collection
   * @return the number of odd numbers in the collection
   */
  private static int oddIntegerCount(List<Integer> collection) {
    int oddIntegerCount = 0;

    for (int i = 0; i < collection.size(); i++) {
      if (collection.get(i) % 2 != 0) {
        oddIntegerCount += 1;
      }
    }
    return oddIntegerCount;
  }

  /**
   * 
   * @param collection
   * @return the number of prime numbers in the collection
   */
  private static int primeIntegerCount(List<Integer> collection) {
    int primeNumberCount = 0;

    for (int i = 0; i < collection.size(); i++) {
      for (int j = 1; j < collection.get(i); j++) {
        if ((j != 1) && (j != collection.get(i)) && ((collection.get(i) % j) == 0)) {
          primeNumberCount += 1;
          break;
        }
      }
    }
    return collection.size() - primeNumberCount;
  }

  /**
   * 
   * @param collection
   * @return the number of strings starting with the letter �c�
   */
  private static int letterCStringCount(List<String> collection) {
    int stringCount = 0;

    for (int i = 0; i < collection.size(); i++) {
      if (collection.get(i).charAt(0) == 'c') {
        stringCount += 1;
      }
    }
    return stringCount;
  }

  /**
   * 
   * @param collection
   * @return the number of Accounts with a balance < 0
   * 
   */
  private static int accountsCount(List<Account> collection) {
    int accountsCount = 0;

    for (int i = 0; i < collection.size(); i++) {
      if (collection.get(i).isOverdraft()) {
        accountsCount += 1;
      }
    }
    return accountsCount;
  }

  public static void doMagicFromCollectionOfAnyType(List<?> collection) {

    System.out.println(">>>>> " + collection);
    // evenIntegerCollection(collection);
    // oddIntegerCollection(collection);
    // primeIntegerCollection(collection);
    // letterCStringCollection(collecti on);
    // accountsCollection(collection);
  }

  public static void main(String[] args) {
    List<Integer> integerCollection = generateIntegerCollection();
    List<String> stringCollection = generateStringCollection();
    List<Account> accountCollection = generateAccountsCollection();

    // Homework 3B
    doMagicFromCollectionOfAnyType(stringCollection);

    // Homework 3A
    System.out.println("EVEN INTEGERS: " + evenIntegerCount(integerCollection));
    System.out.println("ODD INTEGERS: " + oddIntegerCount(integerCollection));
    System.out.println("PRIMES: " + primeIntegerCount(integerCollection));
    System.out.println("STRINGS: " + letterCStringCount(stringCollection));
    System.out.println("ACCOUNTS: " + accountsCount(accountCollection));
  }

  private static List<Account> generateAccountsCollection() {
    List<Account> accountCollection = new ArrayList<>();

    accountCollection.add(new Account("ACC1", 254.0));
    accountCollection.add(new Account("ACC1", 120.0));
    accountCollection.add(new Account("ACC1", -54.0));
    accountCollection.add(new Account("ACC1", -12.0));
    accountCollection.add(new Account("ACC1", 0.0));
    accountCollection.add(new Account("ACC1", 10.0));
    accountCollection.add(new Account("ACC1", -10.0));

    return accountCollection;
  }

  private static List<String> generateStringCollection() {
    List<String> stringCollection = new ArrayList<>();

    stringCollection.add("cat");
    stringCollection.add("dog");
    stringCollection.add("catepillar");
    stringCollection.add("snake");
    stringCollection.add("cow");
    stringCollection.add("elephant");

    return stringCollection;
  }

  public static List<Integer> generateIntegerCollection() {
    List<Integer> integerCollection = new ArrayList<>();

    integerCollection.add(1);
    integerCollection.add(20);
    integerCollection.add(113);
    integerCollection.add(410);
    integerCollection.add(5);
    integerCollection.add(6);
    integerCollection.add(7);
    integerCollection.add(80);
    integerCollection.add(99);
    integerCollection.add(3);
    integerCollection.add(11);
    integerCollection.add(12);
    integerCollection.add(13);

    return integerCollection;
  }
}
