package mpholetsiki.mentorship.generics.homework;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Team {

  private static final int MAX_TEAM_SIZE = 15;

  private List people = new ArrayList();

  public static int maxTeamSize() {
    return MAX_TEAM_SIZE;
  }

  public void addPerson(Object t) {
    people.add(t);
  }

  public void anyMethod(Object t) {
    // do nothing
  }

  public Object getPerson(int position) {
    return people.get(position);
  }

  public int getNumOfPersons() {
    return people.size();
  }

  @SuppressWarnings("unchecked")
  public Object[] asArray(Object[] e) {
    for (int i = 0; i < people.size(); i++) {
      e[i] = (Object) getPerson(i);
    }
    return e;
  }

  public static void main(String[] args) {
    Team team = new Team();
    team.addPerson(new Student("Chris", 40));
    team.addPerson(new Student("Colin", 40));

    for (int i = 0; i < team.getNumOfPersons(); i++) {
      System.out.println(i + " name: " + ((Person) team.getPerson(i)).getName());
      System.out.println(i + " yearregistered: " + ((Student) team.getPerson(i)).getYearRegistered());
    }

    Team t = new Team();

    Student[] students = (Student[]) team.asArray(new Student[2]);

    for (Student student : students) {
      System.out.println("StudentArray: " + student.getName());
    }

    Class clazz = team.getClass();
    try {
      Method method = clazz.getMethod("getPerson", int.class);
    }
    catch (NoSuchMethodException | SecurityException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  // write reflection that g

}
