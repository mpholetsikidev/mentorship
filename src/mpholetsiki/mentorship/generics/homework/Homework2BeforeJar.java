package mpholetsiki.mentorship.generics.homework;

import java.util.ArrayList;
import java.util.List;

public class Homework2BeforeJar {

  public Homework2BeforeJar() {
  }

  public List<Integer> greaterThan1000(List<Integer> numbers) {

    List<Integer> numbersGreaterThan1000 = new ArrayList<Integer>();

    for (int i = 0; i < numbers.size(); i++) {
      if (numbers.get(i) > 1000) {
        numbersGreaterThan1000.add(numbers.get(i));
      }
    }
    return numbersGreaterThan1000;
  }
}
