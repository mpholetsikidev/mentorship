package mpholetsiki.mentorship.generics.homework;

import java.util.ArrayList;
import java.util.List;

public class Homework1C {

  public static void main(String[] args) {
    listReverseWithGenericType();
    listReverseWithRawType();
  }

  private static void listReverseWithGenericType() {
    List<String> someString = new ArrayList<String>();
    someString.add("abc");
    someString.add("def");
    someString.add("ghi");
    someString.add("jkl");

    System.out.println(">>>>> GENERIC TYPE LIST: BEFORE REVERSE");
    for (String listElement : someString) {
      System.out.println(listElement);
    }

    List<String> someReversedString = new ArrayList<String>();
    stringReverseWithGenericType(someString, someReversedString);

    System.out.println(">>>>> GENERIC TYPE LIST: AFTER REVERSE");
    for (String listElement : someReversedString) {
      System.out.println(listElement);
    }
  }

  public static List<String> stringReverseWithGenericType(List<String> listOfStrings, List<String> reversedStringList) {
    // List<String> reversedStringList = new ArrayList<String>();
    for (int i = 0; i < listOfStrings.size(); i++) {
      String newString = "";
      for (int j = listOfStrings.get(i).length() - 1; j >= 0; j--) {
        newString += listOfStrings.get(i).charAt(j);
      }
      reversedStringList.add(i, newString);
    }
    return reversedStringList;
  }

  private static void listReverseWithRawType() {
    List someString = new ArrayList();
    someString.add("abcd");
    someString.add("efgh");
    someString.add("ijkl");
    someString.add("mnop");

    System.out.println(">>>>> RAW TYPE LIST: BEFORE REVERSE");
    for (Object listElement : someString) {
      System.out.println((String) listElement);
    }

    List someReversedString = new ArrayList();
    stringReverseWithRawType(someString, someReversedString);

    // check if jvm uses a foreach or iterator
    // does foreach exist at runtime???
    System.out.println(">>>>> RAW TYPE LIST: AFTER REVERSE");
    for (Object listElement : someReversedString) {
      System.out.println(listElement);
    }
  }

  public static List stringReverseWithRawType(List listOfStrings, List reversedStringList) {
    // List reversedStringList = new ArrayList();
    for (int i = 0; i < listOfStrings.size(); i++) {
      String newString = "";
      for (int j = ((String) listOfStrings.get(i)).length() - 1; j >= 0; j--) {
        newString += ((String) listOfStrings.get(i)).charAt(j);
      }
      reversedStringList.add(i, newString);
    }
    return reversedStringList;
  }
}
