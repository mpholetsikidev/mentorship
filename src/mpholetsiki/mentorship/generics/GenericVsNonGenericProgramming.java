package mpholetsiki.mentorship.generics;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mpholetsiki.javaoo.homework.morph.Employee;

public class GenericVsNonGenericProgramming {

  public static void main(String[] args) {
    nonGeneric();
  }

  /**
   * This how a non generic ArrayList would look like: pros and cons
   */
  public static void generic() {
    List<File> someObjects = new ArrayList<>();

    someObjects.add(new File("myFile"));
    someObjects.add(new File("mySecondFile"));

    // You will not to need to cast when retrieving the contents of the ArrayList, the compiler already made
    // certain that you inserted Objects of one type.
    for (Object object : someObjects) {
      System.out.println(object);
    }
  }

  /**
   * This how a non generic ArrayList would look like: pros and cons
   */
  @SuppressWarnings("unchecked")
  public static void nonGeneric() {
    List someObjects = new ArrayList();

    // someObjects can take in any kind of Object
    someObjects.add(new Employee());
    someObjects.add(123);
    someObjects.add(new File("myFile"));
    someObjects.add("Some String");

    // Retrieving the contents of the ArrayList will give runtime Errors but not compile time errors
    for (Object object : someObjects) {
      System.out.println((Employee) object);
    }
  }
}
