package mpholetsiki.mentorship.lang.collection.collectionsandsorting.myownsortingalgorithm;

public class Customer
    implements Comparable {
  private int customerNum;

  public Customer(int customerNum) {
    this.customerNum = customerNum;
  }

  public int getCustomerNum() {
    return customerNum;
  }

  @Override
  public int compareTo(Object o) {

    Customer cust = (Customer) o;

    if (customerNum > cust.getCustomerNum()) {
      return 1;
    }

    if (customerNum < cust.getCustomerNum()) {
      return -1;
    }
    return 0;
  }
}
