package mpholetsiki.mentorship.lang.collection.collectionsandsorting.myownsortingalgorithm;

import java.util.ArrayList;
import java.util.List;

public class CustomerTest {

  public static void main(String[] args) {
    List<Customer> customers = new ArrayList<>();

    Customer a = new Customer(7);
    Customer b = new Customer(1);
    Customer c = new Customer(3);
    Customer d = new Customer(8);
    Customer e = new Customer(2);
    Customer f = new Customer(9);
    Customer g = new Customer(5);
    customers.add(a);
    customers.add(b);
    customers.add(c);
    customers.add(d);
    customers.add(e);
    customers.add(f);
    customers.add(g);

    System.out.println("Original List");
    for (int i = 0; i < customers.size(); i++) {
      System.out.println(">>>>> " + customers.get(i).getCustomerNum());
    }

    // System.out.println();
    // customers = sortViaNewMethod(customers);
    // System.out.println("Sorted List: Ascending: New Method");
    // for (int i = 0; i < customers.size(); i++) {
    // System.out.println(">>>>> " + customers.get(i).getCustomerNum());
    // }

    System.out.println();
    customers = sort(customers);
    System.out.println("Sorted List: Ascending");
    for (int i = 0; i < customers.size(); i++) {
      System.out.println(">>>>> " + customers.get(i).getCustomerNum());
    }

    System.out.println();
    customers = sortDescending(customers);
    System.out.println("Sorted List: Descending");
    for (int i = 0; i < customers.size(); i++) {
      System.out.println(">>>>> " + customers.get(i).getCustomerNum());
    }
  }

  public static List sort(List<Customer> customers) {
    List<Customer> sortedCustomers = new ArrayList<>();

    // Add first element
    sortedCustomers.add(0, customers.get(0));

    for (int i = 0; i < customers.size(); i++) {
      for (int j = 0; j < i; j++) {
        if (customers.get(i).compareTo(sortedCustomers.get(j)) == 1 && (j + 1) == sortedCustomers.size()) {
          sortedCustomers.add(sortedCustomers.size(), customers.get(i));
        }

        if (customers.get(i).compareTo(sortedCustomers.get(j)) == -1) {
          sortedCustomers.add(j, customers.get(i));
          break;
        }
      }
    }
    return sortedCustomers;
  }

  public static List sortDescending(List<Customer> customers) {
    List<Customer> sortedCustomers = new ArrayList<>();

    // Add first element
    sortedCustomers.add(0, customers.get(0));

    for (int i = 0; i < customers.size(); i++) {
      for (int j = 0; j < i; j++) {
        if (customers.get(i).compareTo(sortedCustomers.get(j)) == -1 && (j + 1) == sortedCustomers.size()) {
          sortedCustomers.add(sortedCustomers.size(), customers.get(i));
        }

        if (customers.get(i).compareTo(sortedCustomers.get(j)) == 1) {
          sortedCustomers.add(j, customers.get(i));
          break;
        }
      }
    }
    return sortedCustomers;
  }

  // public static List sortViaNewMethod(List<Customer> customers) {
  // List<Customer> sortedCustomers = new ArrayList<>();
  // int element = 0;
  // int cust = 0;
  //
  // // Add first element
  // sortedCustomers.add(0, customers.get(0));
  //
  // while (element < customers.size()) {
  //
  // }
  //
  // return sortedCustomers;
  // }
}
