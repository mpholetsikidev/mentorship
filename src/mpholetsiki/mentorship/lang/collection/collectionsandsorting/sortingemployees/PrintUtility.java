package mpholetsiki.mentorship.lang.collection.collectionsandsorting.sortingemployees;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PrintUtility {
  public static void print(Map<Employee, String> employees) {
    for (Entry<Employee, String> employee : employees.entrySet()) {
      System.out.println(employee.getValue());
    }
  }

  public static void print(Set<Employee> employees) {
    for (Employee employee : employees) {
      System.out.println(employee.getEmployeeNum());
    }
  }

  public static void print(List<Employee> employees) {
    for (Employee employee : employees) {
      System.out.println(employee.getEmployeeNum());
    }
  }
}
