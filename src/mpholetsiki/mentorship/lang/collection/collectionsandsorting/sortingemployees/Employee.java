package mpholetsiki.mentorship.lang.collection.collectionsandsorting.sortingemployees;

public class Employee
    implements Comparable {

  private String employeeNum;

  private String name;

  private String surname;

  private double annualSalary;

  private String employeeType;

  public Employee() {

  }

  public Employee(String employeeType, String employeeNum, String name, String surname, double annualSalary) {
    this.employeeNum = employeeNum;
    this.name = name;
    this.surname = surname;
    this.annualSalary = annualSalary;
    this.employeeType = employeeType;
  }

  public String getEmployeeNum() {
    return employeeNum;
  }

  @Override
  public int compareTo(Object o) {
    Employee emp = (Employee) o;
    return employeeNum.compareTo(emp.getEmployeeNum());
  }
}
