package mpholetsiki.mentorship.lang.collection.collectionsandsorting.sortingemployees;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import mpholetsiki.mentorship.lang.collection.collectionsandsorting.sortingemployees.order.MyCollections;

public class EmployeeUtility {

  public static void main(String[] args) {

    Employee e1 = new Employee("Administrator", "A001", "john", "smith", 120000);
    Employee e2 = new Employee("Administrator", "A002", "jane", "doe", 80000);
    Employee e3 = new Employee("Administrator", "A003", "gary", "sinise", 250000);
    Employee e4 = new Employee("Manager", "A010", "craig", "bart", 300000);
    Employee e5 = new Employee("Manager", "A011", "shirley", "norman", 520000);
    Employee e6 = new Employee("Director", "A020", "vincent", "radebe", 600000);
    Employee e7 = new Employee("Director", "A021", "sipho", "msimanga", 1200000);

    Employee[] employeeArray = new Employee[7];
    employeeArray[0] = e1;
    employeeArray[1] = e7;
    employeeArray[2] = e4;
    employeeArray[3] = e5;
    employeeArray[4] = e2;
    employeeArray[5] = e6;
    employeeArray[6] = e3;

    addToHashMap(employeeArray);
    addToTreeMap(employeeArray);
    // addToHashSet(employeeArray);
    // addToTreeSet(employeeArray);
    // addToArrayList(employeeArray);
    // addToLinkedList(employeeArray);

  }

  private static void addToHashMap(Employee... e) {
    /**
     * This proves that order in a HashMap is not defined. The map gets ordered in MyCollections using its keys but
     * prints out the map in a different order each time in the PrintUtility method.
     *
     */
    Map<Employee, String> empAsc = new HashMap<>();
    for (int i = 0; i < e.length; i++) {
      empAsc.put(e[i], e[i].getEmployeeNum());
    }
    MyCollections.sortHashMap(empAsc);
    System.out.println(">>>>> Proving that order of a HashMap is undefined, even after sorting the keys...");
    PrintUtility.print(MyCollections.sortHashMap(empAsc));

    /**
     * A comparator will not work on a HashMap because by definition, a HashMap does not have order.
     * {@code Map<Employee, String> empDesc = new HashMap<Employee, String>(new ComparatorSort()); } will not compile.
     */
    // Map<Employee, String> empDesc = new HashMap<Employee, String>(new ComparatorSort());
    // for (int i = 0; i < e.length; i++) {
    // empDesc.put(e[i], e[i].getEmployeeNum());
    // }
  }

  private static void addToTreeMap(Employee... e) {
    /**
     * A Tree Map will sort elements itself, no need to call a sort on a TreeMap, elements are orders as they are added
     * in the collection.
     */
    Map<Employee, String> empAsc = new TreeMap<>();
    for (int i = 0; i < e.length; i++) {
      empAsc.put(e[i], e[i].getEmployeeNum());
    }
    System.out.println(">>>>> Printing a TreeMap is ascending order...");
    PrintUtility.print(empAsc);

    /**
     * Here is an attempt to reverse the order of a TreeMap using a comparable.
     */
    Map<Employee, String> empDesc = new TreeMap<>(new ComparatorSort());
    for (int i = 0; i < e.length; i++) {
      empDesc.put(e[i], e[i].getEmployeeNum());
    }
    System.out.println(">>>>> Printing a TreeMap is descending order using Comparator...");
    PrintUtility.print(empDesc);
  }
  //
  // private static void addToHashSet(Employee... e) {
  // // Comparable
  // Set<Employee> empAsc = new HashSet<>();
  // for (int i = 0; i < e.length; i++) {
  // empAsc.add(e[i]);
  // }
  //
  // // Comparator
  // Set<Employee> empDesc = new HashSet<>(new ComparatorSort());
  // for (int i = 0; i < e.length; i++) {
  // empDesc.add(e[i]);
  // }
  // }
  //
  // private static void addToTreeSet(Employee... e) {
  // // Comparable
  // Set<Employee> empAsc = new TreeSet<>();
  // for (int i = 0; i < e.length; i++) {
  // empAsc.add(e[i]);
  // }
  //
  // // Comparator
  // Set<Employee> empDesc = new TreeSet<>(new ComparatorSort());
  // for (int i = 0; i < e.length; i++) {
  // empDesc.add(e[i]);
  // }
  // }
  //
  // private static void addToArrayList(Employee... e) {
  // // Comparable
  // List<Employee> empAsc = new ArrayList<>();
  // for (int i = 0; i < e.length; i++) {
  // empAsc.add(e[i]);
  // }
  //
  // // Comparator
  // List<Employee> empDesc = new ArrayList<>(new ComparatorSort());
  // for (int i = 0; i < e.length; i++) {
  // empDesc.add(e[i]);
  // }
  // }
  //
  // private static void addToLinkedList(Employee... e) {
  // // Comparable
  // List<Employee> empAsc = new LinkedList<>();
  // for (int i = 0; i < e.length; i++) {
  // empAsc.add(e[i]);
  // }
  //
  // // Comparator
  // List<Employee> empDesc = new LinkedList<>(new ComparatorSort());
  // for (int i = 0; i < e.length; i++) {
  // empDesc.add(e[i]);
  // }
  // }
}
