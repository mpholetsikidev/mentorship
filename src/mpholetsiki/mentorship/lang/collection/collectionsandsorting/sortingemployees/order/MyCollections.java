package mpholetsiki.mentorship.lang.collection.collectionsandsorting.sortingemployees.order;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mpholetsiki.mentorship.lang.collection.collectionsandsorting.sortingemployees.Employee;

public class MyCollections {

  public static Map sortHashMap(Map elements) {

    List<Employee> sortedKeys = new ArrayList<Employee>();
    Object[] arrayKeys = elements.keySet().toArray();
    for (int i = 0; i < arrayKeys.length; i++) {
      sortedKeys.add((Employee) arrayKeys[i]);
    }

    Collections.sort(sortedKeys);

    Map sortedMap = new HashMap();
    for (int i = 0; i < sortedKeys.size(); i++) {
      sortedMap.put(sortedKeys.get(i), elements.get(sortedKeys.get(i)));
    }

    return sortedMap;
  }

  // public static Map sortMa
}
