package mpholetsiki.mentorship.lang.collection.collectionsandsorting.stringreverse;

import java.util.SortedSet;
import java.util.TreeSet;

public class StringReverse {

  public static void main(String[] args) {
    // unique == Set
    // sorted == SortedSet/TreeSet

    SortedSet stringsAsc = new TreeSet();
    stringsAsc.add("aaa");
    stringsAsc.add("ccc");
    stringsAsc.add("bbb");
    stringsAsc.add("yyy");
    stringsAsc.add("rrr");

    System.out.println(">>>>> Strings sorted in ascending order: ");
    for (Object string : stringsAsc) {
      System.out.println(string);
    }

    SortedSet stringsDesc = new TreeSet(new StringReverseComparator());
    stringsDesc.add("aaa");
    stringsDesc.add("ccc");
    stringsDesc.add("bbb");
    stringsDesc.add("yyy");
    stringsDesc.add("rrr");

    System.out.println(">>>>> Strings sorted in descending order: ");
    for (Object string : stringsDesc) {
      System.out.println(string);
    }
  }
}
