package mpholetsiki.mentorship.lang.collection.collectionsandsorting.stringreverse;

import java.util.Comparator;

public class StringReverseComparator
    implements Comparator {

  @Override
  public int compare(Object o1, Object o2) {

    String s1 = (String) o1;
    String s2 = (String) o2;

    return -1 * (s1.compareTo(s2));
  }
}
