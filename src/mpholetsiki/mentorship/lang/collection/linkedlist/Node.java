package mpholetsiki.mentorship.lang.collection.linkedlist;

/**
 * @author Mpho
 *
 *         This is a basic implementation of a linked list node. It has the data stored in the node. The element has
 *         pointers, which point to a next node. If the element is the last node, the next pointer will point to null.
 */
public class Node {

  private Object data;

  private Node nextPointer;

  private Node previousPointer;

  public Node(Object data, Node nextPointer) {
    this.data = data;
    this.nextPointer = nextPointer;
  }

  public Node(Object data, Node nextPointer, Node previousPointer) {
    this.data = data;
    this.nextPointer = nextPointer;
    this.previousPointer = previousPointer;
  }

  public Node getNode() {
    return this;
  }

  public Object getNodeData() {
    return this.data;
  }

  public Node getNextPointer() {
    return nextPointer;
  }

  public void setNextPointer(Node nextPointer) {
    this.nextPointer = nextPointer;
  }

  public Node getPreviousPointer() {
    return previousPointer;
  }

  public void setPreviousPointer(Node previousPointer) {
    this.previousPointer = previousPointer;
  }

}
