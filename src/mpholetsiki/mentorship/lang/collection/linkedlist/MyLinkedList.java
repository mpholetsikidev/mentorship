package mpholetsiki.mentorship.lang.collection.linkedlist;

/**
 * @author Mpho
 * 
 *         Implementation of a singly linked list.
 */

public class MyLinkedList extends AbstractLinkedList {

  private Node firstNode;

  private Node lastNode;

  private int numberOfNodes = 0;

  /**
   * Constructs an empty list.
   */
  public MyLinkedList() {
  }

  @Override
  public boolean isEmpty() {
    return firstNode == null;
  }

  @Override
  public int size() {
    return numberOfNodes;
  }

  @Override
  public Object addFirst(Object data) {
    if (isEmpty()) {
      firstNode = new Node(data, null);
      incrementListSize();
    }
    else {
      Node newFirstNode = new Node(data, firstNode);

      firstNode.setPreviousPointer(newFirstNode);
      firstNode = newFirstNode;
      incrementListSize();
    }
    return firstNode;
  }

  @Override
  public Object addLast(Object data) {

    if (isEmpty()) {
      return addFirst(data);
    }
    else if (numberOfNodes == 1) { // Only a first node exists
      lastNode = new Node(data, null, firstNode);
      firstNode.setNextPointer(lastNode);
      incrementListSize();
    }
    else {
      lastNode = new Node(data, null, (Node) get(numberOfNodes));
      incrementListSize();
    }

    return lastNode;
  }

  @Override
  public boolean add(Object data) {
    if (lastNode == null) {
      addLast(data);
      return true;
    }
    else {
      Node newNode = new Node(data, null, (Node) get(numberOfNodes));
      lastNode.setNextPointer(newNode);
      lastNode = newNode;
      incrementListSize();
    }
    return false;
  }

  @Override
  public void add(int index, Object data) {

    if (index == 0) {
      addFirst(data);
    }
    else if (index == size()) {
      addLast(data);
    }
    else {
      for (int i = 1; i < size(); i++) {
        if (index == i) {
          Node currentNode = (Node) get(i);
          Node prevNode = currentNode.getPreviousPointer();
          Node newNode = new Node(data, currentNode, prevNode);

          prevNode.setNextPointer(newNode);
          currentNode.setPreviousPointer(newNode);

          incrementListSize();
        }
      }
    }
  }

  @Override
  public Object getFirst() {
    return firstNode;
  }

  @Override
  public Object getLast() {
    return lastNode;
  }

  @Override
  public Object get(int index) {
    if (index == 0) {
      return getFirst();
    }
    else if (index == size()) {
      return getLast();
    }
    else {
      Node node = firstNode.getNextPointer(); // index 1
      for (int i = 1; i < size(); i++) {

        if (index == i) {
          return node;
        }

        node = node.getNextPointer();
      }
    }

    return null;
  }

  @Override
  public void incrementListSize() {
    numberOfNodes++;
  }

  @Override
  public void decrementNodeSize() {
    numberOfNodes--;
  }

  @Override
  public Object removeFirst() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Object removeLast() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Object remove(int index) {
    // TODO Auto-generated method stub
    return null;
  }
}
