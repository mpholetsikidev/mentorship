package mpholetsiki.mentorship.lang.collection.linkedlist;

public interface LinkedList {

  /**
   * Adds the first node to a linked list. Cannot add null as node data.
   */
  public abstract Object addFirst(Object data);

  /**
   * Adds the last node to a linked list and points the previous node to this. If a previous node does not exist, this
   * elements is then added as the first element. Cannot add null as node data.
   */
  public abstract Object addLast(Object data);

  /**
   * Appends a node to a list. The previous node will now point to this added node. Cannot add null as node data.
   */
  public boolean add(Object data);

  /**
   * Adds a node at a specific point in the list. If the index passed through is 0, node is added as a first node, if
   * the index passed through is equal to the index of the last node, the node is added as the last node and the current
   * last node then points to the new last node. This method then points to the next node in the list if it is not null
   * and the previous node will now point to it. Cannot add null as node data.
   */
  public void add(int index, Object data);

  /**
   * Retrieves the first node.
   */
  public Object getFirst();

  /**
   * Retrieves te last node.
   */
  public Object getLast();

  /**
   * Retrieves a node at a specific point in the list.
   */
  public Object get(int index);

  /**
   * Removes a node at a specific point in the list.
   */
  public Object remove(int index);

  /**
   * Removes the first node.
   */
  public Object removeFirst();

  /**
   * Removes the last node.
   */
  public Object removeLast();

  /**
   * Increments the size of the list if a node is added.
   */
  public abstract void incrementListSize();

  /**
   * decrements the size of the list if a node is being deleted.
   */
  public abstract void decrementNodeSize();

  /**
   * Check if a list is empty Returns true if it is. Returns false if it has data.
   */
  public boolean isEmpty();

  /**
   * This returns the size of the Linked List. The size is the number of nodes the list has.
   */
  public int size();
}
