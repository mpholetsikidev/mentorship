package mpholetsiki.mentorship.lang.collection.linkedlist;

public class TestLinkedList {

  public static MyLinkedList linkedList;

  public static String testStatus;

  public static void main(String[] args) {

    testCreateAnEmptyLinkedList();
    testAddingTheFirstNode();
    testAddingTheLastNode();
    testAddingTheLastNodeIfThereIsNoFirstNode();
    testAddingANewNodeIfTheresAFirstNodeAndNoLastNode();
    testAddingANewNodeIfThereIsNoFirstAndNoLastNode();
    testAddingANewNodeIfThereIsAFirstAndLastNode();
    testAddingANodeAtAspecificIndex();
    testGettingANodeAtASpecificIndex();

    testRemovingAFirstNode();
    testRemovingALastNode();
    testRemovingANodeAtAspecificIndex();

    getTestStatus();
  }

  private static void testRemovingANodeAtAspecificIndex() {
    // TODO Auto-generated method stub

  }

  private static void testRemovingALastNode() {
    // TODO Auto-generated method stub

  }

  private static void testRemovingAFirstNode() {
    // TODO Auto-generated method stub

  }

  private static void testGettingANodeAtASpecificIndex() {
    linkedList = getNewEmptyLinkedList();
    Object firstNode = linkedList.addFirst("node1");
    Object lastNode = linkedList.addLast("node2");
    Object node = linkedList.add("just any node1");
    Object node1 = linkedList.add("just any node2");

    linkedList.get(0);
    linkedList.get(1);
    linkedList.get(2);
    linkedList.get(3);
  }

  private static void testAddingANodeAtAspecificIndex() {
    int index = 1;
    linkedList = getNewEmptyLinkedList();
    Object firstNode = linkedList.addFirst("node1");
    Object lastNode = linkedList.addLast("node2");
    Object node = linkedList.add("just any node1");
    Object node1 = linkedList.add("just any node2");

    linkedList.add(index, "just any node added");

  }

  private static void testAddingTheFirstNode() {
    linkedList = getNewEmptyLinkedList();
    Object firstNode = linkedList.addFirst("MyFirstNode");

    if (firstNode == null) {
      failTest("First node not added", "testAddingTheFirstNode");
    }
  }

  private static void testAddingTheLastNode() {
    linkedList = getNewEmptyLinkedList();
    Object firstNode = linkedList.addFirst("MyFirstNode");
    Object lastNode = linkedList.addLast("MyLastNode");

    if (lastNode == null) {
      failTest("Last node not added", "testAddingTheLastNode");
    }
  }

  private static void testAddingTheLastNodeIfThereIsNoFirstNode() {
    linkedList = getNewEmptyLinkedList();
    Object lastNode = linkedList.addLast("MyLastNode");

    if (lastNode == null) {
      failTest("Last node not added", "testAddingTheLastNodeIfThereIsNoFirstNode");
    }
  }

  private static void testAddingANewNodeIfTheresAFirstNodeAndNoLastNode() {
    linkedList = getNewEmptyLinkedList();

    Object firstNode = linkedList.addFirst("MyFirstNode");
    Object node = linkedList.add("just any node");

    if (node == null) {
      failTest("Node not added", "testAddingANewNodeIfTheresAFirstNodeAndNoLastNode");
    }
  }

  private static void testAddingANewNodeIfThereIsNoFirstAndNoLastNode() {
    linkedList = getNewEmptyLinkedList();

    Object node = linkedList.add("just any node");

    if (node == null) {
      failTest("Node not added", "testAddingANewNodeIfThereIsNoFirstAndNoLastNode");
    }
  }

  private static void testAddingANewNodeIfThereIsAFirstAndLastNode() {
    linkedList = getNewEmptyLinkedList();

    Object firstNode = linkedList.addFirst("node1");
    Object lastNode = linkedList.addLast("node2");
    Object node = linkedList.add("just any node");

    if (node == null) {
      failTest("Node not added", "testAddingANewNodeIfThereIsAFirstAndLastNode");
    }
  }

  private static void testCreateAnEmptyLinkedList() {
    linkedList = getNewEmptyLinkedList();

    if (!linkedList.isEmpty()) {
      failTest("Linked List is not empty", "testCreateAnEmptyLinkedList");
    }
  }

  public static MyLinkedList getNewEmptyLinkedList() {
    MyLinkedList linkedList = new MyLinkedList();
    return linkedList;
  }

  public static void failTest(String error, String method) {
    testStatus = "fail";
    String message = "\n" + error + ": (" + method + ")";
    throw new RuntimeException(message);
  }

  public static void getTestStatus() {
    if (testStatus != "fail") {
      System.out.println("All Tests Passed...");
    }
  }
}
