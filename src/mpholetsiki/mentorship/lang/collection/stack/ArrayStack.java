package mpholetsiki.mentorship.lang.collection.stack;

import java.util.Iterator;

public class ArrayStack extends AbstractStack {

  private Object[] arrayStack;

  private int currentPosition = 0;

  private final static int DEFAULT_CAPACITY = 10;

  private final static int SIZE_TO_INCREMENT = 10;

  public ArrayStack() {
    arrayStack = new Object[DEFAULT_CAPACITY];
  }

  public ArrayStack(int initialCapacity) {
    arrayStack = new Object[initialCapacity];
  }

  @Override
  public void push(Object obj) {

    if (currentPosition >= arrayStack.length) {
      expandArrayStack();
    }
    arrayStack[currentPosition] = obj;

    currentPosition += 1;

    // this does the ++ after it has added an object to the current position
    // arrayStack[currentPosition++] = obj;
  }

  @Override
  public Object pop() {
    Object removedStack = arrayStack[currentPosition];
    arrayStack[currentPosition] = null;
    currentPosition -= 1;

    return removedStack;
  }

  @Override
  public Object get(int position) {
    if (position < currentPosition) {
      return arrayStack[position];
    }
    else {
      throw new NullPointerException();
    }
  }

  @Override
  public boolean add(Object e) {
    try {
      push(e);
      return true;
    }
    catch (Exception ex) {
      return false;
    }
  }

  @Override
  public boolean contains(Object o) {
    for (int i = currentPosition; i >= 0; i--) {
      if (o.equals(arrayStack[i])) {
        return true;
      }
    }

    return false;
  }

  @Override
  public boolean isEmpty() {
    return currentPosition != 0;
  }

  @Override
  public int size() {
    int currentSize = currentPosition - 1;
    if (currentSize == -1) {
      return 0;
    }

    return currentSize;
  }

  @Override
  public Iterator iterator() {
    return new StackIterator(this);
  }

  @Override
  public void clear() {
    arrayStack = null;
    currentPosition = 0;
  }

  private void expandArrayStack() {

    int newSize = currentPosition + SIZE_TO_INCREMENT;

    Object[] tempArrayStack = new Object[newSize];

    for (int i = 0; i < currentPosition; i++) {
      tempArrayStack[i] = arrayStack[i];
    }

    this.arrayStack = tempArrayStack;

  }
}
