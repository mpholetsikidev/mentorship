package mpholetsiki.mentorship.lang.collection.stack;

import java.util.Iterator;

import javax.management.RuntimeErrorException;

public class TestStack {

  public static void main(String[] args) {
    // testStackIsEmpty();
    testIfStackIsEmpty();
    testTheStackIterator();
    testIfTheStackCanPopAnElement();
    testIfTheStackCanGetAnElementInACertainPosition(1);
    testIfStackCanAddAnElement();
    testIfStackContainsAnElement();
    testAddingMoreThanTenItemsToStack();
    testClearingTheStack();
  }

  private static void testStackIsEmpty() {
    Stack stack = getNewStack();
    if (!stack.isEmpty()) {
      throw new RuntimeErrorException(null, "Stack is empty");
    }
  }

  private static Stack getNewStack() {
    Stack stack = new ArrayStack();
    return stack;
  }

  private static void testTheStackIterator() {

    Stack stack = getNewStack();
    testPushingAnElementIntoAStack(stack);

    System.out.println("_____iterator()_____\nIterating through stack using Iterator");
    for (Iterator stackIterator = stack.iterator(); stackIterator.hasNext();) {
      System.out.println(stackIterator.next());
    }
    System.out.println();
  }

  private static void testClearingTheStack() {

    Stack stack = getNewStack();
    testPushingAnElementIntoAStack(stack);

    System.out.println("_____clear()_____\nCreating a stack and clearing all the elements");
    System.out.println("Size before calling clear(): " + stack.size());
    stack.clear();
    System.out.println("Size after calling clear(): " + stack.size());
    System.out.println();
  }

  private static void testAddingMoreThanTenItemsToStack() {

    Stack stack = getNewStack();
    System.out.println("_____ add() _____\nSize before adding any items: " + stack.size());
    stack.push("item0");
    stack.push("item1");
    stack.push("item2");
    stack.push("item3");
    stack.push("item4");
    stack.push("item5");
    stack.push("item6");
    stack.push("item7");
    stack.push("item8");
    stack.push("item9");
    stack.push("item10");
    stack.push("item11");
    stack.push("item12");
    stack.push("item13");
    stack.push("item14");
    System.out.println("Size after adding 15 items: " + stack.size());
    System.out.println();
  }

  private static void testIfStackContainsAnElement() {

    Stack stack = getNewStack();
    testPushingAnElementIntoAStack(stack);

    System.out.println("_____contains()_____\nChecking if a Stack contains an elements: ");
    stack.push("testContains");
    System.out.println("Testing an element that is in the list: " + stack.contains("testContains"));
    System.out
        .println("Testing an element that is not in the list: " + stack.contains("testContainsShouldreturnFalse"));
    System.out.println();
  }

  private static void testIfStackCanAddAnElement() {

    Stack stack = getNewStack();
    testPushingAnElementIntoAStack(stack);

    System.out.println("_____add()_____\nAdding an extra elements to stack using add() method");
    System.out.println("Size before calling add(): " + stack.size());
    stack.add("testAdd1");
    System.out.println("Size after calling add(): " + stack.size());
    System.out.println();
  }

  private static void testIfTheStackCanPopAnElement() {

    Stack stack = getNewStack();
    testPushingAnElementIntoAStack(stack);

    System.out.println("_____pop()_____\nCreating a stack and removing the last element by calling pop() method");
    System.out.println("Size before calling pop(): " + stack.size());
    stack.pop();
    System.out.println("Size after calling pop(): " + stack.size());
    System.out.println();
  }

  private static void testIfTheStackCanGetAnElementInACertainPosition(int i) {

    Stack stack = getNewStack();
    testPushingAnElementIntoAStack(stack);

    System.out.println("_____get(i)_____\nGetting an item at a certain index");
    System.out.println("get(" + i + "): " + stack.get(i));
    System.out.println();
  }

  private static void testPushingAnElementIntoAStack(Stack stack) {
    stack.push("item0");
    stack.push("item1");
    stack.push("item2");
    stack.push("item3");
    stack.push("item4");
    System.out.println("Pushed items. New stack size: " + stack.size());
  }

  private static void testIfStackIsEmpty() {
    System.out.println("_____isEmpty()_____\nTest if a Stack is empty before and after adding elements: ");
    Stack stack = getNewStack();
    System.out.println("Before adding elements: " + stack.isEmpty());
    testPushingAnElementIntoAStack(stack);
    System.out.println("After adding elements: " + stack.isEmpty());
    System.out.println();
  }
}