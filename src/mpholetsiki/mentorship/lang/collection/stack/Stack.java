/**
 * It can contain duplicates
 * It naturally orders from top to bottom, and new Objects are always added to the top - thus, iteration through the Stack will go from top to bottom
 * As a Stack works a lot with the top object, the operations push() and pop() apply 
 */

package mpholetsiki.mentorship.lang.collection.stack;

import java.util.Collection;

public interface Stack
    extends Collection {

  public abstract void push(Object obj);

  public abstract Object pop();

  public abstract Object get(int position);
}