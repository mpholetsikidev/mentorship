package mpholetsiki.mentorship.lang.collection.stack;

import java.util.Iterator;

public class StackIterator
    implements Iterator {

  private int current = 0;

  private Stack stack;

  public StackIterator(Stack stack) {
    this.stack = stack;
  }

  @Override
  public boolean hasNext() {
    return current != stack.size() + 1;
  }

  @Override
  public Object next() {
    Object obj = null;
    if (hasNext()) {
      obj = stack.get(current);
    }
    current += 1;
    return obj;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
