package mpholetsiki.mentorship.lang.collection.sortedstack;

import java.util.Collection;
import java.util.Iterator;

/**
 * A collection that contains duplicates, and supports LIFO (Last In, First Out) concept.
 * 
 * @author Mpho
 */
public interface Stack
    extends Collection {

  /**
   * Inserts an element at the top of the stack.
   */
  public abstract void push(Object obj);

  /**
   * Removes the element at the top of the stack.
   */
  public abstract Object pop();

  /**
   * Retrieves the element by an index in the stack. Cannot remove the element if it is not the top most
   * element in the stack.
   */
  public abstract Object get(int position);

  /**
   * @return the number of elements in this stack.
   */
  int size();

  /**
   * @return true if there are no elements in the stack
   * @return false if there are elements added in to the stack
   */
  boolean isEmpty();

  /**
   * Removes all the elements in the stack.
   * The stack will be empty after this method is called.
   */
  void clear();

  /**
   * Compares the current stack with another stack for equality.
   * 
   * @return true if the objects are equal.
   */
  boolean equals(Object o);

  /**
   * Returns the hash code value for this stack.
   */
  int hashCode();

  /**
   * Returns an iterator over the elements in this stack
   */
  Iterator iterator();
}
