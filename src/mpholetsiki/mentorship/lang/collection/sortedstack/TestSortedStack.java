package mpholetsiki.mentorship.lang.collection.sortedstack;

public class TestSortedStack {
  private static String testStatus;

  private static SortedStack stack;

  public static void main(String[] args) {
    runTests();
  }

  private static void runTests() {
    testCreatingAnEmptySortedStackShouldReturnEmptyStack();
    testPushOneObjectIntoSortedStack();
    getTestStatus();
  }

  private static void testPushOneObjectIntoSortedStack() {
    stack = getNewSortedStack();
    stack.push("abc");
    if (stack.size() != 1) {
      failTest("Stack push failed", "testPushOneObjectIntoSortedStack");
    }
  }

  private static void testCreatingAnEmptySortedStackShouldReturnEmptyStack() {
    stack = getNewSortedStack();
    if (!stack.isEmpty()) {
      failTest("Stack is not empty", "testCreatingAnEmptySortedStackShouldReturnEmptyStack");
    }
  }

  public static SortedStack getNewSortedStack() {
    SortedStack stack = new TreeStack(5);
    return stack;
  }

  public static void failTest(String error, String method) {
    testStatus = "fail";
    String message = "\n" + error + ": (" + method + ")";
    throw new RuntimeException(message);
  }

  public static void getTestStatus() {
    if (testStatus != "fail") {
      System.out.println("All Tests Passed...");
    }
  }
}
