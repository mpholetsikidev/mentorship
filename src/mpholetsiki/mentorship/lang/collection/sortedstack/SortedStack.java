/**
 * This set provides an ordered stack in ascending order.
 * Elements are ordered as they are added to the stack.
 */

package mpholetsiki.mentorship.lang.collection.sortedstack;

public interface SortedStack
    extends Stack, Comparable {

  /**
   * Compares this object with the specified object for order. Returns a negative integer, zero, or a positive integer
   * as this object is less than, equal to, or greater than the specified object.
   */
  @Override
  public int compareTo(Object o);

}