package mpholetsiki.mentorship.lang.collection.sortedstack;

import java.util.Collection;
import java.util.Iterator;

public class TreeStack
    implements SortedStack {

  private Object[] sortedStack;

  private int currentPosition = 0;

  public TreeStack(int initialCapacity) {
    sortedStack = new Object[initialCapacity];
  }

  @Override
  public void push(Object obj) {
    // Do i sort as i push??? or do i run sort/compareTo after pushing????
    sortedStack[0] = obj;
    incrementCurrentPosition();
  }

  private void incrementCurrentPosition() {
    currentPosition++;
  }

  @Override
  public Object pop() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Object get(int position) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public int size() {
    return currentPosition;
  }

  @Override
  public boolean isEmpty() {
    return currentPosition == 0;
  }

  @Override
  public void clear() {
    // TODO Auto-generated method stub

  }

  @Override
  public Iterator iterator() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean contains(Object o) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Object[] toArray() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Object[] toArray(Object[] a) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean add(Object e) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean remove(Object o) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean addAll(Collection c) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean retainAll(Collection c) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public int compareTo(Object o) {
    // TODO Auto-generated method stub
    return 0;
  }

}
