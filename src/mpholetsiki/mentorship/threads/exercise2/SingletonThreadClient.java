package mpholetsiki.mentorship.threads.exercise2;

public class SingletonThreadClient {

  private static Singleton instance1;

  private static Singleton instance2;

  public static void main(String[] args)
      throws InterruptedException {
    Thread newThread = new Thread(new Runnable() {

      public void run() {
        instance2 = Singleton.getInstance();
      }

    });
    newThread.start();
    // TimeUnit.SECONDS.sleep(1);
    instance1 = Singleton.getInstance();
    newThread.join();

    System.out.println(instance1 == instance2);
  }
}
