package mpholetsiki.mentorship.threads.exercise2;

public class Singleton {

  // private static final Singleton instance = new Singleton();
  // class loading happens once and is atomic.

  private static Singleton instance;

  private Singleton() {
    System.out.println("Constructing Singleton");
  }

  // public static Singleton getInstance() {
  // return instance;
  // }

  public static Singleton getInstance() {
    if (instance == null) {
      synchronized (Singleton.class) {
        if (instance == null) {
          instance = new Singleton();
        }
      }
    }
    return instance;
  }
}
