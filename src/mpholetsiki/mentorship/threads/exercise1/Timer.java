package mpholetsiki.mentorship.threads.exercise1;

public class Timer {

  public static long start() {
    return System.currentTimeMillis();
  }

  public static float end(long timeStarted) {
    return (System.currentTimeMillis() - timeStarted) / 1000f;
  }
}