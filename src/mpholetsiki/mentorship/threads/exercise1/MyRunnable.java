package mpholetsiki.mentorship.threads.exercise1;

public class MyRunnable
    implements Runnable {

  private int start;

  private int end;

  private int total;

  public MyRunnable(int start, int end) {
    this.start = start;
    this.end = end;
  }

  @Override
  public void run() {
    calculateTotal();
  }

  public void calculateTotal() {
    for (int i = start; i <= end; i++) {
      total += i;
    }
  }

  public int getTotal() {
    return total;
  }
}