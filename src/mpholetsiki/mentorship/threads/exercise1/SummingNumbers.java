package mpholetsiki.mentorship.threads.exercise1;

public class SummingNumbers {

  private static int number = 100;

  private static int threadCount = 3;

  public static void main(String[] args) {
    sumNumbersWithoutAThread();
    sumNumbersWithTwoThreads();
  }

  private static void sumNumbersWithoutAThread() {
    long time = Timer.start();

    MySumClass sum = new MySumClass(number);
    sum.calculateSum();
    System.out.println(">>>>> sumNumbersWithoutAThread: " + sum.getTotal());
    System.out.println(">>>>> Time taken: " + Timer.end(time) + " seconds\n");
  }

  private static void sumNumbersWithTwoThreads() {

    long time = Timer.start();
    int total = 0;
    int min = 0;
    int max = number;
    int range = number / threadCount;

    for (int i = 0; i < threadCount; i++) {

      min = getMin(max, i);
      max = getMax(min, range, i);

      MyRunnable r = new MyRunnable(min, max);
      Thread thread = new Thread(r);
      thread.start();

      try {
        thread.join();
        total += r.getTotal();
      }
      catch (InterruptedException e) {
        e.printStackTrace();
        System.out.println("Thread join() was Interrupted");
      }
    }

    System.out.println(">>>>> sumNumbersWithTwoThreads: " + total);
    System.out.println(">>>>> Time taken: " + Timer.end(time) + " seconds");
  }

  private static int getMax(int min, int range, int i) {
    int max;
    if (i == 0) {
      max = number / threadCount;
    }
    else {
      max = min + range;
      if (max > number) {
        max = number;
      }
    }
    return max;
  }

  private static int getMin(int max, int i) {
    if (i == 0) {
      return 1;
    }
    else {
      return max + 1;
    }
  }
}
