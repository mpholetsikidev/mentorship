package mpholetsiki.mentorship.threads.exercise1;

public class MySumClass {

  private int number;

  private int total;

  public MySumClass(int number) {
    this.number = number;
  }

  public void calculateSum() {
    for (int i = 0; i <= number; i++) {
      total += i;
    }
  }

  public int getTotal() {
    return total;
  }
}