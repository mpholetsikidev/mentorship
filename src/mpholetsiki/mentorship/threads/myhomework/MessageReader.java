package mpholetsiki.mentorship.threads.myhomework;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class MessageReader
    implements Runnable {

  private static int messageCheckInterval = 3;

  @Override
  public void run() {
    // thread t never ends
    // while (true) {
    // check messages every three seconds
    for (;;) {
      try {
        if (MessageWriter.getMessageTotal() > 0) {
          List<String> messages = new MessageWriter().getMessages();

          for (String message : messages) {
            System.out.println(">>>>> Reader " + message);
          }
          // MessageWriter.resetMessageTotal();
        }

        TimeUnit.SECONDS.sleep(messageCheckInterval);
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    // }
  }

  public void startClient() {
    Thread t = new Thread(new MessageReader());
    t.start();
  }
}
