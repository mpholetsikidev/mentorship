package mpholetsiki.mentorship.threads.myhomework;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MessageWriter
    implements Runnable {

  private List<String> messages = new ArrayList<>();

  private static int numberOfMessages = 0;

  private static int numberOfMessagesToPopulate = 12;

  @Override
  public void run() {
    addMessage();
  }

  private synchronized void addMessage() {
    messages.add("message" + numberOfMessages);
    numberOfMessages += 1;
  }

  public void writeMessage() {

    for (int i = 0; i < numberOfMessagesToPopulate; i++) {
      Thread t = new Thread(new MessageWriter());
      t.start();

      if (i == numberOfMessagesToPopulate / 2 || i == numberOfMessagesToPopulate / 3
          || i == numberOfMessagesToPopulate - 1) {
        try {
          TimeUnit.SECONDS.sleep(3);
        }
        catch (InterruptedException e) {
          // noop
          e.printStackTrace();
        }
      }
    }
  }

  public static void resetMessageTotal() {
    numberOfMessages = 0;
  }

  public static int getMessageTotal() {
    return numberOfMessages;
  }

  public List<String> getMessages() {
    return messages;
  }
}
