package mpholetsiki.mentorship.threads.myhomework;

public class MessageClient {

  public static void main(String[] args) {
    startMessageReader();
    startMessageWriter();
  }

  private static void startMessageWriter() {
    MessageWriter messageWriter = new MessageWriter();
    messageWriter.writeMessage();
  }

  private static void startMessageReader() {
    MessageReader messageReader = new MessageReader();
    messageReader.startClient();
  }
}
