package mpholetsiki.mentorship.threads.homework;

import java.util.List;

/**
 * 
 * @since 19 May 2011
 */
public class MessageReader extends Thread {

  private static Messages messages = Messages.getInstance();

  private Object lock;

  public MessageReader(Object lock) {
    this.lock = lock;
  }

  @Override
  public void run() {
    readMessages();
  }

  public void readMessages() {
    while (true) {
      synchronized (lock) {
        while (!messages.hasMessages()) {
          try {
            System.out.println("MessageReader: Waiting for messages...");
            System.out.println(">>>>> MessageReader: " + lock);
            lock.wait(); // note, wait releases the lock, and waits for notify
          }
          catch (InterruptedException ex) {
            System.out.println("Interrupted");
          }
          List<String> messages = Messages.getInstance().readMessages();
          System.out.println(messages);
        }
      }
    }
  }

}
