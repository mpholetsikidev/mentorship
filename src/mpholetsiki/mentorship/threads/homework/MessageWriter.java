package mpholetsiki.mentorship.threads.homework;

import java.util.concurrent.TimeUnit;

/**
 * 
 * @since 19 May 2011
 */
public class MessageWriter extends Thread {

  private static Messages messages = Messages.getInstance();

  private int messageNum = 0;

  @Override
  public void run() {
    writeMessages();
  }

  public void writeMessages() {
    String message = null;
    try {
      for (int j = 1; j <= 3; j++) {
        TimeUnit.SECONDS.sleep(1);
        synchronized (this) {
          for (int i = 1; i <= 200; i++) {
            message = "Message num : " + ++messageNum;
            messages.writeMessage(message);
          }
          System.out.println("MessageWriter: Completed writing messages...");
          TimeUnit.SECONDS.sleep(1);
          System.out.println(">>>>> MessageWriter: " + this);
          notify();
        }
      }
    }
    catch (InterruptedException ex) {
      // do nothing
    }
  }

  public static void main(String[] args)
      throws InterruptedException {
    Thread t1 = new MessageWriter();
    Thread t2 = new MessageReader(t1);
    t2.start();
    TimeUnit.SECONDS.sleep(1);
    t1.start();
    t1.start();
  }
}
