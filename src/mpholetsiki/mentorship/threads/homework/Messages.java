package mpholetsiki.mentorship.threads.homework;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @since 19 May 2011
 */
public class Messages {

  private static Messages instance = new Messages();

  private List<String> messages = new ArrayList<String>();

  public static Messages getInstance() {
    return instance;
  }

  public synchronized void writeMessage(String message) {
    messages.add(message);
  }

  public synchronized List<String> readMessages() {
    List<String> result = new ArrayList<String>();
    for (Iterator<String> i = messages.iterator(); i.hasNext();) {
      String message = i.next();
      result.add(message);
      i.remove();
    }
    return result;
  }

  public boolean hasMessages() {
    return messages.size() > 0;
  }

}
