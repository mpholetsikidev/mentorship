package mpholetsiki.mentorship.polymorphism;

import java.util.ArrayList;
import java.util.List;

public class Client {
  public static void main(String[] args) {

  }

  public void addShapes(Shape shape) {
    List<Shape> shapes = new ArrayList<Shape>();
    shapes.add(shape);

    Shape.print(shape);
  }
}
