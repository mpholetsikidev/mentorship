package mpholetsiki.mentorship.oo;

public class OOExample {

  private int year;

  private int month;

  private int day;

  public int days;

  private int[] DAYS_IN_MONTH = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  // public OOExample(int year, int month, int day) {
  // super();
  // this.year = year;
  // this.month = month;
  // this.day = day;
  // }

  public OOExample(int year, int month, int day, int days) {
    super();
    this.year = year;
    this.month = month;
    this.day = day;
    this.days = days;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public int getDay() {
    return day;
  }

  public void setDay(int day) {
    this.day = day;
  }

  public int getDays() {
    return days;
  }

  public void setDays(int days) {
    this.days = days;
  }

  public void validateDate()
      throws InvalideDateException {
    if (this.getYear() > 2015) {
      throw new InvalideDateException();
    }

    if (this.getMonth() < 0 && this.getMonth() > 12) {
      throw new InvalideDateException();
    }

    if (this.getDay() > DAYS_IN_MONTH[this.getMonth()] && this.getDay() < 0) {
      throw new InvalideDateException();
    }
  }

  public void addDaysToDate() {
    int daysAdded = this.day + this.days;

    while (daysAdded > DAYS_IN_MONTH[this.month]) {
      daysAdded = day - DAYS_IN_MONTH[this.month];
      this.month = this.month + 1;
    }
  }

  public void compareDate(int year, int month, int day) {
    // this.year;
    // this.month;
    // this.day;

  }
}