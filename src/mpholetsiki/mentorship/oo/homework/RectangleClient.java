package mpholetsiki.mentorship.oo.homework;

public class RectangleClient {
  public static void main(String[] args) {
    Rectangle rectA = new Rectangle(19, 12);
    Rectangle rectB = new Rectangle(19, 12);
    Rectangle rectC = new Rectangle(64, 34);
    Rectangle rectD = new Rectangle(45, 45);
    Rectangle rectE = new Rectangle(198, 56);

    System.out.println("\n");
    System.out.println("rectA Area: " + rectA.getArea());
    System.out.println("rectA Parameter: " + rectA.getParameter());
    System.out.println("rectA Diagonal: " + rectA.getDiagonal());
    System.out.println("");

    checkIdentity(rectA, rectB, rectC);
    checkEquality(rectA, rectB, rectC);

    // What is the key difference between each object?
    // -- They have different lengths and widths
    // What is the same for each object?
    // -- They are all passing through two parameters to Rectangle Object
  }

  private static void checkIdentity(Rectangle rectA, Rectangle rectB, Rectangle rectC) {
    System.out.println("rectA(19, 12) == rectB(19, 12) returns " + (rectA == rectB));
    System.out.println("rectB(19, 12) == rectC(64, 34) returns " + (rectB == rectC));
  }

  private static void checkEquality(Rectangle rectA, Rectangle rectB, Rectangle rectC) {
    System.out.println("rectA(19, 12).equals(rectB(19, 12)) returns " + (rectA.equals(rectB)));
    System.out.println("rectB(19, 12).equals(rectC(64, 34)) returns " + (rectB.equals(rectC)));
  }
}
