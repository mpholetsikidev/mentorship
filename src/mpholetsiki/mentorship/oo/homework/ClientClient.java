package mpholetsiki.mentorship.oo.homework;

public class ClientClient {
  public static void main(String[] args) {
    Client a = new Client("1n", "Mpho", "123");
    Client b = new Client("2n", "Letsiki", "123");
    Client c = new Client("3c", "Apply Soap", "123");
    Client d = new Client("4c", "Mixed Berries", "123");
    Client e = new Client("5c", "Rectangle", "123");

    System.out.println("Object a >>>>> " + a.getClientType());
    System.out.println("Object c >>>>> " + c.getClientType());
  }
}
