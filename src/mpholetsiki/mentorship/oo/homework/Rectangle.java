package mpholetsiki.mentorship.oo.homework;

public class Rectangle {

  private int length;

  private int width;

  private static int degree = 90;

  public Rectangle(int length, int width) {
    super();

    try {
      this.length = length;
      this.width = width;

      validateLength(length);
      System.out.println("Is rectangle a square: " + isRectangleASquare(this.length, this.width));
    }
    catch (InvalidRectangleLengthException e) {
      e.printStackTrace();
    }
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public static void validateLength(int length)
      throws InvalidRectangleLengthException {
    if (length > 100 && !(length < 0)) {
      throw new InvalidRectangleLengthException();
    }
  }

  public static boolean isRectangleASquare(int length, int width) {
    if (length == width) {
      degree = 90;
    }
    return length == width;
  }

  public int getArea() {
    return this.length * this.width;
  }

  public int getParameter() {
    return (2 * this.length) + (2 * this.width);
  }

  public double getDiagonal() {
    return Math.sqrt(this.width + this.length);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) return false; // non-null rule
    if (this == obj) return true; // reflexive
    if (this.getClass() != obj.getClass()) { // checks if the object passed through is of type Rectangle
      return false;
    }

    Rectangle rect = (Rectangle) obj; // cast will always pass
    if (length == rect.length && width == rect.width) {
      return true;
    }
    return false;
  }
}
