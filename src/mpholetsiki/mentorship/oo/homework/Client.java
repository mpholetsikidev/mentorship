package mpholetsiki.mentorship.oo.homework;

public class Client {

  private String customerNum;

  private String name;

  private String telNum;

  public Client(String customerNum, String name, String telNum) {
    super();
    this.customerNum = customerNum;
    this.name = name;
    this.telNum = telNum;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public void setCustomerNum(String customerNum) {
    this.customerNum = customerNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTelNum() {
    return telNum;
  }

  public void setTelNum(String telNum) {
    this.telNum = telNum;
  }

  public String getClientType() {

    if (this.customerNum.contains("n")) {
      return "Natural Person";
    }
    else if (this.customerNum.contains("c")) {
      return "Company";
    }

    return "Type not defined";
  }

  public boolean checkEquality() {
    // noop
    return false;
  }

  public boolean checkIdentity() {
    // noop
    return false;
  }
}
