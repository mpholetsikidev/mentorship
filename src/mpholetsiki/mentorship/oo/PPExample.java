package mpholetsiki.mentorship.oo;

public class PPExample {

  private static int[] DAYS_IN_MONTH = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  public static void main(String[] args)
      throws InvalideDateException {
    validateDate(2012, 3, 12);
    addDaysToDate(2012, 3, 12, 2);
    compareDates();
  }

  public static void validateDate(int year, int month, int day)
      throws InvalideDateException {
    if (year > 2015) {
      throw new InvalideDateException();
    }

    if (month < 0 && month > 12) {
      throw new InvalideDateException();
    }

    if ((day > DAYS_IN_MONTH[month]) && day < 0) {
      throw new InvalideDateException();
    }
  }

  public static void addDaysToDate(int year, int month, int day, int daysToAdd) {
    int daysAdded = day + daysToAdd;

    while (daysAdded > DAYS_IN_MONTH[month]) {
      daysAdded = day - DAYS_IN_MONTH[month];
      month = month + 1;
    }

    System.out.println("New Date: " + year + "," + daysAdded + "," + month);
  }

  public static void compareDates() {

  }
}
