package mpholetsiki.mentorship.oo;

public class OOExampleMain {
  public static void main(String[] args)
      throws InvalideDateException {
    OOExample oo = new OOExample(2011, 3, 12, 0);
    oo.validateDate();
    oo.setDays(5);
    oo.addDaysToDate();
  }
}
