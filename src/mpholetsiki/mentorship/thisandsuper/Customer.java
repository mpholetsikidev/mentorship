package mpholetsiki.mentorship.thisandsuper;

public class Customer extends Person {

  public Customer(int yearOfBirth) {
    this(yearOfBirth, true);
    System.out.println(">>>>> In Customer Class Constructor 1");
  }

  public Customer(int yearOfBirth, boolean activeCustomer) {
    super(yearOfBirth);
    System.out.println(">>>>> In Customer Class Constructor 2");
  }

  // @Override
  // public Person(int yearOfBirth) {
  // System.out.println(">>>>> In Person Class Constructor 1");
  // }

  @Override
  public int getAge(int currentYear) {
    int age = super.getAge(currentYear);

    // super(1990);
    // Person(1990);

    getYearOfBirth();

    return age;
  }

  public int getYearsToPay(int minYears) {
    return minYears + 2;
  }

  @Override
  public String toString() {
    System.out.println(">>>>> this is the toString in Customer.java: " + super.getClass().getName());
    return super.toString();
  }
}
