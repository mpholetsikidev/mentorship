package mpholetsiki.mentorship.thisandsuper;

public class Person extends GodClass {

  private int yearOfBirth;

  public Person(int yearOfBirth) {
    this(true, yearOfBirth);
    System.out.println(">>>>> In Person Class Constructor 1");
  }

  public Person(boolean isPerson, int yearOfBirth) {
    this.yearOfBirth = yearOfBirth;
    System.out.println(">>>>> In Person Class Constructor 2");
  }

  public int getAge(int currentYear) {
    return currentYear - yearOfBirth;
  }

  public int getYearOfBirth() {
    return yearOfBirth;
  }

  @Override
  public String toString() {
    System.out.println(">>>>> this is the toString in Person.java: " + super.toString().getClass().getName());
    // return super.toString() + " >>> this is the toString() from Person.java, not Object.java";
    return super.toString();
  }
}
