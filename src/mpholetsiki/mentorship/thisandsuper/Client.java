package mpholetsiki.mentorship.thisandsuper;

public class Client {

  public static void main(String[] args) {
    Customer cust = new Customer(1990);
    System.out.println("Customer age: " + cust.getAge(2015));
    System.out.println("Years To Pay Money: " + cust.getYearsToPay(10));
    System.out.println("To String" + cust.toString());
  }
}
